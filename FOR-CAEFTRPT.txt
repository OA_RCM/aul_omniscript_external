************************************************************************************************
**
**    External Text File Name: FOR-CAEFTRPT.txt
**
**    OMNI is somehow drafting client's accounts on FOR adjustment codes.
**    This report needs to run before 2:00 pm to catch them.
**
**    It is run external.
**
**    Variable List:
**       TX011  Plan
**       TX012  Previous plan number
**
**       WK001  Error Flag           
**       WK002  Page number of report
**       WK003  Line number of report
**       WK004  CA Forfeiture amt
**	
**    Created by                           Date         CC 
**    ==============================     ========     ========
**    Rick Sica                          04/25/2005   CC8286
**
**    Modification History:
**
**    Programmer                           Date         SMR
**    ==============================     ========     ========
**    Judy Stewart                       11/10/2006   cc9709
**    Reason :   Renamed this ONE-SHOT to a standard program name.  One shots should not
**               be running in production more than one time.
************************************************************************************************
OCPAGE_SETUP(PLAN:'000001' RPTSEQ:999 FORM:'0002');     
SD080=100000;
WK002=1;
PLPLOBJ_VIEW(PLANLO:'A00000');
LOOP WHILE PLPLOBJ_NEXT();
   TX011=PLPLOBJ_DE(011);
   CACAOBJ_VIEW(PLAN:TX011);
   LOOP WHILE CACAOBJ_NEXT();
      IF CACAOBJ_DE(110)='Y';
         IF CACAOBJ_DE(250)='Y';
            WK004=CACAOBJ_NUMDE(430);
            IF WK004>0;
               PERFORM 'WRITE.REPORT';
            END;
         END; 
      END;
   ENDLOOP;
ENDLOOP;
IF WK003>0;
   OCPAGE_WRITE(RPTSEQ:999);         
END;
********************************************************************************
ROUTINE 'WRITE.REPORT';
IF WK003=0;
   OCPAGE_SET('CA EFT Forfeitures'  LINE:1   COL:33);                
   OCPAGE_SET('PAGE: '                   LINE:1   COL:71);
   OCPAGE_SET(OCFMT(WK002 'F,5')         LINE:1   COL:76);
   OCPAGE_SET('PLAN  '                   LINE:3   COL:1);
   OCPAGE_SET('CCA ID'                   LINE:3   COL:8);
   OCPAGE_SET('Created'                  LINE:3   COL:27);
   OCPAGE_SET('ET Trade Dt'              LINE:3   COL:38);
   OCPAGE_SET('Forfeiture Amt'           LINE:3   COL:49);
   OCPAGE_SET('   Deposit Amt'           LINE:3   COL:65);
   OCPAGE_SET('  Load/CTL Amt'           LINE:3   COL:81);
   OCPAGE_SET('------'                   LINE:4   COL:1);
   OCPAGE_SET('------------------'       LINE:4   COL:8);
   OCPAGE_SET('----------'               LINE:4   COL:27);
   OCPAGE_SET('-----------'              LINE:4   COL:38);
   OCPAGE_SET('--------------'           LINE:4   COL:49);
   OCPAGE_SET('   -----------'           LINE:4   COL:65);
   OCPAGE_SET('  ------------'           LINE:4   COL:81);
   WK003=5;
   WK002=WK002+1;
END;
IF TX011<>TX012;
   OCPAGE_SET(TX011   LINE:WK003  COL:1   LENG:6); 
   TX012=TX011;
END;
OCPAGE_SET(CACAOBJ_DE(010)                      LINE:WK003  COL:8   LENG:18);
OCPAGE_SET(OCDATE_MMDDYYYY(CACAOBJ_NUMDE(140))  LINE:WK003  COL:27  LENG:10);
OCPAGE_SET(OCDATE_MMDDYYYY(CACAOBJ_NUMDE(775))  LINE:WK003  COL:38  LENG:10);
OCPAGE_SET(OCFMT(WK004 'F,11.2')                LINE:WK003  COL:49  LENG:15);
OCPAGE_SET(OCFMT(CACAOBJ_NUMDE(400) 'F,11.2')   LINE:WK003  COL:65  LENG:15);
OCPAGE_SET(OCFMT(CACAOBJ_NUMDE(480) 'F,11.2')   LINE:WK003  COL:81  LENG:15);
WK003=+1;
IF WK003>51;
   WK003=0;
   TX012=' ';
   OCPAGE_WRITE(RPTSEQ:999);         
END;
GOBACK;
