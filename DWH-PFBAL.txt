************************************************************************************
*
*  Text File Name: DWH-PFBAL.txt
*
*  Create an extract of PF data elements for active plans on a passed in planlistfile
*
*  Created by                           Date         WMS
*  ==============================     ========     ========
*  Judy Stewart                       03/01/12      WMS5501
*
*
*  Variable List:
*
*    WK001=#PARTS WITH A BALANCE FOR PLAN        TX001=PLANLIST FILENAME ($PLANLISTFILE)
*    WK002=TOTAL #PARTS WITH A BAL ALL PLANS     TX002=PLANLIST RECORD STRING
*    WK003=PF units found                        TX003=PLAN
*    WK004=ERROR FLAG FOR INPUT PARAMETERS       TX004=OUTPUT FILENAME ($DWHOUT)
*    WK005=OUT EXT FILE WRITE STATUS CONDITION   TX005=OUTPUT FILE RECORD STRING (TEMP)
*    WK008=                                      TX008=
*    WK009=                                      TX009=
*    WK010=                                      TX010=
*    WK011=NO LINES WRITTEN TO OUTPUT FILE       TX011=
*    WK012=                                      TX012=
*    WK013=1 year less from Run Date             TX013=KEY value
*    WK014=                                      TX014=
*    WK020=                                      TX020=
*    WK021=                                      TX021=
*    WK022= # TRANSACTIONS QUALIFIED             TX022=
*    WK099= REPORT LINE COUNTER                  TX023=
*
*  Modification History:
*  Programmer           Date
*  ==============     ========     ========
*  Rick Sica          10/20/2014   11030     Excluded Terminated plans; efficiency
*************************************************************************************
SD080=99999999;
TX004=OCTEXT_GETENV('DWHOUT');
IF TX004='';
   RPLOG_INFO(MSG:('OUTPUT FILE NAME BLANK --- PLEASE SET $DWHOUT AND RERUN!!!'));
   WK004=+1;
ELSE;
   IF OCFILV2_OPEN(NAME:TX004 MODE:'OUTPUT') = 0;
      RPLOG_WARN(MSG:('AUL - DWH-PFBAL.txt - DWHOUT does not exist/creating : '+TX004));
   END;
END;
TX001=OCTEXT_GETENV('PLANLISTFILE');
IF TX001='';
   RPLOG_INFO(MSG:('PLANLIST FILENAME BLANK -- PLEASE SET PLANLISTFILE AND RERUN!!!'));
   WK004=+1;
END;
IF WK004>0;    /* Inputs missing */
   RPLOG_ERROR(MSG:('ALL EXPECTED INPUT PARAMETERS ARE NOT SET -- SEE ABOVE.'));
END;
OCSHOW_LINE('DWH-PFBAL PARAMETERS');
OCSHOW_LINE('====================================');
OCSHOW_LINE('$DWHOUT       = ' TX004);
OCSHOW_LINE('$PLANLISTFILE = ' TX001);
OCSHOW_LINE('====================================');
IF OCFILE1_OPEN(NAME:TX001 MODE:'INPUT')=0;
   RPLOG_WARN(MSG:('AUL - DWH-PFBAL.txt - $PLANLISTFILE does not exist/exiting : ' + TX001));
ELSE;
   WK013=OCDATE_BUMP(SD003 YEAR:-1);
   LOOP WHILE OCFILE1_READ(INTO:TX002);                 /* READ FILE LOOP */
      OCCSV_SETLINE(TX002);
      OCCSV_VIEW();
      TX003=OCCSV_FIELD(1);                              /* PLAN */
      IF ((TX003<>'') AND (OCSUB(TX003 1 1)<>'0'));      /* PLAN NOT BLANK AND NOT GLOBAL */
         IF PLPLOBJ_GET(PLAN:TX003)>0;
            WK014=PLPLOBJ_NUMDE(804);
            IF (PLPLOBJ_DE(621)='0') OR (WK014=0) OR (WK014>=WK013);
               PTPHOBJ_VIEW(PLAN:TX003);
               LOOP WHILE PTPHOBJ_NEXT();
                  WK003=0;
                  PTPFOBJ_VIEW(PLAN:TX003 PARTID:PTPHOBJ_DE(007));
                  LOOP WHILE PTPFOBJ_NEXT() AND (WK003=0);
                     IF PTPFOBJ_NUMDE(130)>0;
                        WK003=1;WK001=+1;
                     END;
                  ENDLOOP;
               ENDLOOP;
               WK002=+WK001;
               TX013='PLPL011'+SDTAB+TX003; /* here */
               TX006=OCFMT_INTEGER5(WK001); /* here */
               TX005='PART_COUNT_BALANCE'+SDTAB+OCTEXT_TRIM(TX006)+SDTAB+OCTEXT_TRIM(TX013); /* here */
               PERFORM 'WRITE-OUTPUT';
               WK001=0;
            END;
         END;
      END;           /* PLAN NOT BLANK END */
   ENDLOOP;
END;               /* FILE OPEN OKAY */
OCFILE1_CLOSE();   /* CLOSE files */
OCFILV2_CLOSE();   
**
OCSHOW_LINE('TOTAL #OF PARTS with a $balance : ' OCFMT_INTEGER5(WK002));
OCSHOW_LINE('TOTAL #OF lines written to file : ' OCFMT_INTEGER5(WK011));
**
*******************************************************************************************************
ROUTINE 'WRITE-OUTPUT';
*******************************************************************************************************
IF OCFILV2_WRITE(TX005)=0;
   RPLOG_ERROR(MSG:('AUL: DWH-PFBAL could not write parts with a balance count to OUTPUT EXTRACT FILE: '+TX005));
   QUIT;
ELSE;
   WK011=+1;  /* NO OF LINES WRITTEN */
END;
