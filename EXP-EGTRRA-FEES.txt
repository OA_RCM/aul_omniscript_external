********************************************************************************
**
**    EXP-EGTRRA-FEES (T247)                                         WMS1519B
**
**    Output: Create T988 with fee amount from input file
**
**
**    CREATED BY        DATE     WMS
**    ==============  ========  ======
**    Glen McPherson  01/08/09     572
**
**    MODIFICATION HISTORY:
**
**    PROGRAMMER        DATE      CC    REASON
**    ==============  ========  ======  ========================================
**    Judy Stewart  04/20/2009 WMS1519B Omniplus 5.8 Upgrade - Omniscripts
**    Glen McPherson 10/02/09  WMS1519  5.8 Upgrade
**    Danny Hagans   3/19/2010 WMS3879  Change code to pickup a file daily
********************************************************************************
**PARM SECTION.
**TEXT SECTION.
**CALC SECTION.
SD080=9999999;
WK001=0;
* WK021=OCDATE_MONTHEND(WK023);       /* Last day of month */
* WK022=OCDATE_MONTHENDDAY(WK023);    /* Number days in month */
* WK022=WK022-1;
* WK023=WK021-WK022;                  /* Get first day of month */
* TX004=OCTEXT_GETENV('AULDATA') + '/DATEDAILY';
WK023=OCDATE_CURRENT();
TX004=OCSUB(OCFMT(WK023 'Z9') 2 8);
IF OCFILE2_OPEN(NAME:TX004 MODE:'INPUT');
   TX004=''; TX004=OCFILE2_READ();  /* Read date */
   OCFILE2_CLOSE();
END;
WK023=OCTEXT_TONUM(OCSUB(TX004 1 8));
OCSHOW_LINE('TRADE DATE ' OCFMT(WK023));
TX030=OCTEXT_GETENV('POSTOUT');
TX030=TX030+"/EGTRRA"+OCDATE_FORMAT(WK023 "YYYYMMDD")+".csv"; /* WMS1519B */
OCSHOW_LINE("Input File: " TX030);
IF OCFILE1_OPEN(NAME:TX030 MODE:'INPUT')=0;
   RPLOG_WARN(MSG:('AUL -  INPUT FILE OPEN ERROR'));
   QUIT;
END;
LOOP WHILE OCFILE1_READ(INTO:TX012); /* Read file loop */
    OCCSV_SETLINE(TX012);
    OCCSV_VIEW();
    TX010=OCCSV_FIELD(1);            /* Plan */
    OCSHOW_LINE("PLAN= " TX010);
    WK010=OCCSV_FIELDN(2);           /* Fee Amount */
    PLPLOBJ_VIEW(PLAN:TX010);
    LOOP WHILE PLPLOBJ_NEXT();
       TX005=OCFMT_DATE3(WK023)+'.S988';
       BATIUT_INIT(PLAN:TX010 PARTID:'000000000' TRAN:'988' DATE:WK023);
       BATIUT_SETDE(100 VALUE:('EXP-EGTRRA(FEE:'+OCFMT(WK010 'Z,9.2')+');'));
       BATIUT_ADDTRAN();
       BATIUT_TOVTRAN(FILEID:TX005 SYSTEM:'PROD' STEP:'M4');
    ENDLOOP;
ENDLOOP;
