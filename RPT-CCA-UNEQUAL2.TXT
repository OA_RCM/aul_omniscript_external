*************************************************************************************************************************
**       RPT-CCA-UNEQUAL2: Omniscript designed to generate a report of CCA's in Omni's Cash Control data sets that     **
**                         do not have matching Cash History entries that are closed.                                  **
**                                                                                                                     **
**       TX  VARIABLE LIST:   TX001 -  Plan Number                                                                     **
**                            TX002 -  Team Code                                                                       **
**                            TX003 -  CCA ID                                                                          **
**                            TX004 -  Create User Id                                                                  **
**                            TX005 -  OCSHOW field                                                                    **
**                            TX006 -  File Id                                                                         **
**                            TX007 -  Unstring Plan Number                                                            **
**                            TX008 -  Unstring Team Code                                                              **
**                            TX009 -  Unstring File ID                                                                **
**                            TX010 -  Unstring Create User Id                                                         **
**                            TX011 -  Unstring Cash Total                                                             **
**                            TX012 -  Unstring Trans Total                                                            **
**                            TX013 -  Unstring Difference                                                             **
**                            TX014 -  OCSHOW field                                                                    **
**                            TX015 -  FILEID                                                                          **
**                            TX016 -  Hold Plan Number                                                                **
**                            TX017 -  Hold File ID                                                                    **
**                            TX018 -  Unit identifier (DSB, IFD, OSU or TSS) (WMS 6307)                               **
**                            TX019 -                                                                                  **
**                            TX020 -  OCWRITE String Field                                                            **
**                            TX021 -  Current Date Sting                                                              **
**                            TX022 -  Page Number String                                                              **
**                            TX023 -  Time Stamp String                                                               **
**                            TX090 -  File location string                                                            **
**                            TX099 -  Open, Read and Write error msg field                                            **
**                                                                                                                     **
**       WK  VARIABLE LIST:   WK001 -  Current Date                                                                    **
**                            WK002 -  Current Time and CCA Create Date                                                **
**                            WK003 -  CCA Total Deposit                                                               **
**                            WK004 -                                                                                  **
**                            WK005 -  CCH Amount Accumulated and Line Counter                                         **
**                            WK006 -  Page Counter                                                                    **
**                                                                                                                     **
** MODIFICATION LOG:                                                                                                   **
**                                                                                                                     **
**   Date       Programmer     WMS       Description                                                                   **
** ---------  --------------- --------   ------------------------------------                                          **
** 06/09/11   Gary Pieratt     6307      Split report between departments                                              **
*************************************************************************************************************************
SD080=999999999;
TX090=OCTEXT_GETENV('XJOBSV') + '/cca_unequal.srt';
IF OCFILE1_OPEN(NAME:TX090 MODE:'INPUT')=0;
   OCSHOW_LINE('OPEN ERROR INPUT ' TX090);
   QUIT;
END;
TX090=OCTEXT_GETENV('XJOBSV') + '/' + OCTEXT_GETENV('EBSRPTPFX') + 'DEPNETRANS';
IF OCFILE2_OPEN(NAME:TX090 MODE:'OUTPUT')=0;
   OCSHOW_LINE('OPEN ERROR OUTPUT ' TX090);
   QUIT;
END;
TX090=OCTEXT_GETENV('XJOBSV') + '/' + OCTEXT_GETENV('EBSRPTPFX') + 'DEPNETRANS.DSB';
IF OCFILE3_OPEN(NAME:TX090 MODE:'OUTPUT')=0;
   OCSHOW_LINE('OPEN ERROR OUTPUT ' TX090);
   QUIT;
END;
TX090=OCTEXT_GETENV('XJOBSV') + '/' + OCTEXT_GETENV('EBSRPTPFX') + 'DEPNETRANS.OSU';
IF OCFILE4_OPEN(NAME:TX090 MODE:'OUTPUT')=0;
   OCSHOW_LINE('OPEN ERROR OUTPUT ' TX090);
   QUIT;
END;
TX090=OCTEXT_GETENV('XJOBSV') + '/' + OCTEXT_GETENV('EBSRPTPFX') + 'DEPNETRANS.TS';
IF OCFILE5_OPEN(NAME:TX090 MODE:'OUTPUT')=0;
   OCSHOW_LINE('OPEN ERROR OUTPUT ' TX090);
   QUIT;
END;
TX090=OCTEXT_GETENV('XJOBSV') + '/' + OCTEXT_GETENV('EBSRPTPFX') + 'DEPNETRANS.csv';
IF OCFILE6_OPEN(NAME:TX090 MODE:'OUTPUT')=0;
   OCSHOW_LINE('OPEN ERROR OUTPUT ' TX090);
   QUIT;
END;
TX090=OCTEXT_GETENV('XJOBSV') + '/' + OCTEXT_GETENV('EBSRPTPFX') + 'DEPNETRANS.DSB.csv';
IF OCFILE7_OPEN(NAME:TX090 MODE:'OUTPUT')=0;
   OCSHOW_LINE('OPEN ERROR OUTPUT ' TX090);
   QUIT;
END;
TX090=OCTEXT_GETENV('XJOBSV') + '/' + OCTEXT_GETENV('EBSRPTPFX') + 'DEPNETRANS.OSU.csv';
IF OCFILE8_OPEN(NAME:TX090 MODE:'OUTPUT')=0;
   OCSHOW_LINE('OPEN ERROR OUTPUT ' TX090);
   QUIT;
END;
TX090=OCTEXT_GETENV('XJOBSV') + '/' + OCTEXT_GETENV('EBSRPTPFX') + 'DEPNETRANS.TS.csv';
IF OCFILE9_OPEN(NAME:TX090 MODE:'OUTPUT')=0;
   OCSHOW_LINE('OPEN ERROR OUTPUT ' TX090);
   QUIT;
END;
TX021=OCDATE_MMDDYYYY(SD003);
TX023=OCTIME_HHMM2(OCTIME_CURRENT());
TX029=OCDATE_MMDDYYYY(OCDATE_CURRENT());
*
PERFORM 'CSV.HEADING';
PERFORM 'CSV.HEADING2';
PERFORM 'CSV.HEADING3';
PERFORM 'CSV.HEADING4';
TX016=''; TX019=''; TX025=''; TX027='';
TX017=''; TX024=''; TX026=''; TX028='';
WK005=66; WK007=66; WK009=66; WK011=66; WK013=66;
WK006=0;  WK008=0;  WK010=0;  WK012=0;  WK014=0;
LOOP WHILE OCFILE1_READ(INTO:TX006);
   TX007=OCSUB(TX006 1 6);
   TX008=OCSUB(TX006 8 2);
   TX009=OCSUB(TX006 11 18);
   TX010=OCSUB(TX006 30 14);
   TX011=OCSUB(TX006 53 17);
   TX012=OCSUB(TX006 73 17);
   TX013=OCSUB(TX006 93 17);
   TX015=OCSUB(TX006 111 22);
   TX018=OCSUB(TX006 134 3);
   TX014=TX007+','+TX008+','+TX009+','+TX010+',"'+TX011+'",'+TX015+',"'+TX012+'","'+TX013+'"';
   IF TX018 = 'IFD';
      IF WK005 > 60;
         WK006=+1;
         PERFORM 'REPORT.HEADING';
      END;
      IF (TX007 <> TX016) OR (TX008 <> TX017);
         PERFORM 'PLAN.CHANGE';
      ELSE;
         PERFORM 'PLAN.SAME';
      END;
      OCFILE6_WRITE(TX014);
      TX016=TX007;
      TX017=TX008;
   ELSEIF TX018 = 'DSB';
          IF WK007 > 60;
             WK008=+1;
             PERFORM 'REPORT.HEADING2';
          END;
          IF (TX007 <> TX019) OR (TX008 <> TX024);
             PERFORM 'PLAN.CHANGE2';
          ELSE;
             PERFORM 'PLAN.SAME2';
          END;
          OCFILE7_WRITE(TX014);
          TX019=TX007;
          TX024=TX008;
       ELSEIF TX018 = 'OSU';
              IF WK009 > 60;
                 WK010=+1;
                 PERFORM 'REPORT.HEADING3';
              END;
              IF (TX007 <> TX025) OR (TX008 <> TX026);
                 PERFORM 'PLAN.CHANGE3';
              ELSE;
                 PERFORM 'PLAN.SAME3';
              END;
              OCFILE8_WRITE(TX014);
              TX025=TX007;
              TX026=TX008;
           ELSEIF TX018 = 'TSS';
                  IF WK011 > 60;
                     WK012=+1;
                     PERFORM 'REPORT.HEADING4';
                  END;
                  IF (TX007 <> TX027) OR (TX008 <> TX028);
                     PERFORM 'PLAN.CHANGE4';
                  ELSE;
                     PERFORM 'PLAN.SAME4';
                  END;
                  OCFILE9_WRITE(TX014);
                  TX027=TX007;
                  TX028=TX008;
   END;
ENDLOOP;
TX007=' NONE ';
TX008='**';
TX009='******************';
TX010='**************';
TX011='*****************';
TX012='*****************';
TX013='*****************';
IF WK006=0;
   WK006=1;
   PERFORM 'REPORT.HEADING';
   PERFORM 'PLAN.CHANGE';
END;
IF WK008=0;
   WK008=1;
   PERFORM 'REPORT.HEADING2';
   PERFORM 'PLAN.CHANGE2';
END;
IF WK010=0;
   WK010=1;
   PERFORM 'REPORT.HEADING3';
   PERFORM 'PLAN.CHANGE3';
END;
IF WK012=0;
   WK012=1;
   PERFORM 'REPORT.HEADING4';
   PERFORM 'PLAN.CHANGE4';
END;
*
OCFILE1_CLOSE();
OCFILE2_CLOSE();
OCFILE3_CLOSE();
OCFILE4_CLOSE();
OCFILE5_CLOSE();
*
**********************************************************************************************************************************************************************************************
* ROUTINES
**********************************************************************************************************************************************************************************************
ROUTINE 'PLAN.CHANGE';
TX020= '  PLAN ' + TX007 + ' TEAM ' + TX008 + '   CCA ID             ' + ' USER ID       ' + '       CASH TOTAL     ' + ' FOLDER NAME        ' + '   TRANS TOTAL     ' + '   DIFFERENCE      ';
OCFILE2_WRITE(TX020);
TX020= '                        ' + TX009 + '  ' + TX010 + ' ' + TX011+ '     NO CTL FOUND       ';
OCFILE2_WRITE(TX020);
TX020= '                 SUB TOTAL                                 ' + TX011 + '                      ' + TX012 + ' ' + TX013;
OCFILE2_WRITE(TX020);
TX020=' ';
OCFILE2_WRITE(TX020);
WK005=+4;
GOBACK;
***********************************************************************************************************************************************************************************************
ROUTINE 'PLAN.SAME';
TX020= '                        ' + TX009 + '  ' + TX010 + ' ' + TX011+ '     NO CTL FOUND     ';
OCFILE2_WRITE(TX020);
TX020= '                 SUB TOTAL                                 ' + TX011 + '                      ' + TX012 + ' ' + TX013;
OCFILE2_WRITE(TX020);
TX020=' ';
OCFILE2_WRITE(TX020);
WK005=+3;
GOBACK;
***********************************************************************************************************************************************************************************************
ROUTINE 'REPORT.HEADING';
TX022= OCTEXT_TRIM(OCFMT(INTEGER5:WK006));
TX020='1 RUN DATE: ' + TX029 + ' ' + TX023 + '                                 AMERICAN UNITED LIFE - OMNI                            PAGE: ' + TX022;
OCFILE2_WRITE(TX020);
TX020='  CYCLE DATE: ' + TX021 + '                            OPEN CCA REPORT - DEPOSITS/ TRANSACTIONS UNEQUAL                 RPT-CCA-UNEQUAL';
OCFILE2_WRITE(TX020);
TX020=' ';
OCFILE2_WRITE(TX020);
WK005=3;
GOBACK;
************************************************************************************************************************************************************************************************
ROUTINE 'PLAN.CHANGE2';
TX020= '  PLAN ' + TX007 + ' TEAM ' + TX008 + '   CCA ID             ' + ' USER ID       ' + '       CASH TOTAL     ' + ' FOLDER NAME        ' + '   TRANS TOTAL     ' + '   DIFFERENCE      ';
OCFILE3_WRITE(TX020);
TX020= '                        ' + TX009 + '  ' + TX010 + ' ' + TX011+ '     NO CTL FOUND       ';
OCFILE3_WRITE(TX020);
TX020= '                 SUB TOTAL                                 ' + TX011 + '                      ' + TX012 + ' ' + TX013;
OCFILE3_WRITE(TX020);
TX020=' ';
OCFILE3_WRITE(TX020);
WK007=+4;
GOBACK;
***********************************************************************************************************************************************************************************************
ROUTINE 'PLAN.SAME2';
TX020= '                        ' + TX009 + '  ' + TX010 + ' ' + TX011+ '     NO CTL FOUND     ';
OCFILE3_WRITE(TX020);
TX020= '                 SUB TOTAL                                 ' + TX011 + '                      ' + TX012 + ' ' + TX013;
OCFILE3_WRITE(TX020);
TX020=' ';
OCFILE3_WRITE(TX020);
WK007=+3;
GOBACK;
***********************************************************************************************************************************************************************************************
ROUTINE 'REPORT.HEADING2';
TX022= OCTEXT_TRIM(OCFMT(INTEGER5:WK008));
TX020='1 RUN DATE: ' + TX029 + ' ' + TX023 + '                                  AMERICAN UNITED LIFE - OMNI                            PAGE: ' + TX022;
OCFILE3_WRITE(TX020);
TX020='  CYCLE DATE: ' + TX021 + '                            OPEN CCA REPORT - DEPOSITS/ TRANSACTIONS UNEQUAL                 RPT-CCA-UNEQUAL';
OCFILE3_WRITE(TX020);
TX020=' ';
OCFILE3_WRITE(TX020);
WK007=3;
GOBACK;
************************************************************************************************************************************************************************************************
ROUTINE 'PLAN.CHANGE3';
TX020= '  PLAN ' + TX007 + ' TEAM ' + TX008 + '   CCA ID             ' + ' USER ID       ' + '       CASH TOTAL     ' + ' FOLDER NAME        ' + '   TRANS TOTAL     ' + '   DIFFERENCE      ';
OCFILE4_WRITE(TX020);
TX020= '                        ' + TX009 + '  ' + TX010 + ' ' + TX011+ '     NO CTL FOUND       ';
OCFILE4_WRITE(TX020);
TX020= '                 SUB TOTAL                                 ' + TX011 + '                      ' + TX012 + ' ' + TX013;
OCFILE4_WRITE(TX020);
TX020=' ';
OCFILE4_WRITE(TX020);
WK009=+4;
GOBACK;
***********************************************************************************************************************************************************************************************
ROUTINE 'PLAN.SAME3';
TX020= '                        ' + TX009 + '  ' + TX010 + ' ' + TX011+ '     NO CTL FOUND     ';
OCFILE4_WRITE(TX020);
TX020= '                 SUB TOTAL                                 ' + TX011 + '                      ' + TX012 + ' ' + TX013;
OCFILE4_WRITE(TX020);
TX020=' ';
OCFILE4_WRITE(TX020);
WK005=+9;
GOBACK;
***********************************************************************************************************************************************************************************************
ROUTINE 'REPORT.HEADING3';
TX022= OCTEXT_TRIM(OCFMT(INTEGER5:WK010));
TX020='1 RUN DATE: ' + TX029 + ' ' + TX023 + '                                 AMERICAN UNITED LIFE - OMNI                            PAGE: ' + TX022;
OCFILE4_WRITE(TX020);
TX020='  CYCLE DATE: ' + TX021 + '                            OPEN CCA REPORT - DEPOSITS/ TRANSACTIONS UNEQUAL                 RPT-CCA-UNEQUAL';
OCFILE4_WRITE(TX020);
TX020=' ';
OCFILE4_WRITE(TX020);
WK009=3;
GOBACK;
************************************************************************************************************************************************************************************************
ROUTINE 'PLAN.CHANGE4';
TX020= '  PLAN ' + TX007 + ' TEAM ' + TX008 + '   CCA ID             ' + ' USER ID       ' + '       CASH TOTAL     ' + ' FOLDER NAME        ' + '   TRANS TOTAL     ' + '   DIFFERENCE      ';
OCFILE5_WRITE(TX020);
TX020= '                        ' + TX009 + '  ' + TX010 + ' ' + TX011+ '     NO CTL FOUND       ';
OCFILE5_WRITE(TX020);
TX020= '                 SUB TOTAL                                 ' + TX011 + '                      ' + TX012 + ' ' + TX013;
OCFILE5_WRITE(TX020);
TX020=' ';
OCFILE5_WRITE(TX020);
WK011=+4;
GOBACK;
***********************************************************************************************************************************************************************************************
ROUTINE 'PLAN.SAME4';
TX020= '                        ' + TX009 + '  ' + TX010 + ' ' + TX011+ '     NO CTL FOUND     ';
OCFILE5_WRITE(TX020);
TX020= '                 SUB TOTAL                                 ' + TX011 + '                      ' + TX012 + ' ' + TX013;
OCFILE5_WRITE(TX020);
TX020=' ';
OCFILE5_WRITE(TX020);
WK011=+3;
GOBACK;
***********************************************************************************************************************************************************************************************
ROUTINE 'REPORT.HEADING4';
TX022= OCTEXT_TRIM(OCFMT(INTEGER5:WK012));
TX020='1 RUN DATE: ' + TX029 + ' ' + TX023 + '                                 AMERICAN UNITED LIFE - OMNI                            PAGE: ' + TX022;
OCFILE5_WRITE(TX020);
TX020='  CYCLE DATE: ' + TX021 + '                            OPEN CCA REPORT - DEPOSITS/ TRANSACTIONS UNEQUAL                 RPT-CCA-UNEQUAL';
OCFILE5_WRITE(TX020);
TX020=' ';
OCFILE5_WRITE(TX020);
WK011=3;
GOBACK;
************************************************************************************************************************************************************************************************
ROUTINE 'CSV.HEADING';
*TX022= OCTEXT_TRIM(OCFMT(INTEGER5:WK006));
TX020='RUN DATE: ' + TX029 + ' ' + TX023 + ',AMERICAN UNITED LIFE - OMNI,PAGE: ' + TX022;
OCFILE6_WRITE(TX020);
TX020='CYCLE DATE: ' + TX021 + ',OPEN CCA REPORT - DEPOSITS/ TRANSACTIONS UNEQUAL,RPT-CCA-UNEQUAL';
OCFILE6_WRITE(TX020);
TX020= 'PLAN,TEAM,CCA ID,USER ID,CASH TOTAL,FOLDER NAME,TRANS TOTAL,DIFFERENCE';
OCFILE6_WRITE(TX020);
GOBACK;
*
ROUTINE 'CSV.HEADING2';
TX020='RUN DATE: ' + TX029 + ' ' + TX023 + ',AMERICAN UNITED LIFE - OMNI';
OCFILE7_WRITE(TX020);
TX020='CYCLE DATE: ' + TX021 + ',OPEN CCA REPORT - DEPOSITS/ TRANSACTIONS UNEQUAL,RPT-CCA-UNEQUAL';
OCFILE7_WRITE(TX020);
TX020= 'PLAN,TEAM,CCA ID,USER ID,CASH TOTAL,FOLDER NAME,TRANS TOTAL,DIFFERENCE';
OCFILE7_WRITE(TX020);
GOBACK;
*
ROUTINE 'CSV.HEADING3';
TX020='RUN DATE: ' + TX029 + ' ' + TX023 + ',AMERICAN UNITED LIFE - OMNI';
OCFILE8_WRITE(TX020);
TX020='CYCLE DATE: ' + TX021 + ',OPEN CCA REPORT - DEPOSITS/ TRANSACTIONS UNEQUAL,RPT-CCA-UNEQUAL';
OCFILE8_WRITE(TX020);
TX020= 'PLAN,TEAM,CCA ID,USER ID,CASH TOTAL,FOLDER NAME,TRANS TOTAL,DIFFERENCE';
OCFILE8_WRITE(TX020);
GOBACK;
*
ROUTINE 'CSV.HEADING4';
TX020='RUN DATE: ' + TX029 + ' ' + TX023 + ',AMERICAN UNITED LIFE - OMNI';
OCFILE9_WRITE(TX020);
TX020='CYCLE DATE: ' + TX021 + ',OPEN CCA REPORT - DEPOSITS/ TRANSACTIONS UNEQUAL,RPT-CCA-UNEQUAL';
OCFILE9_WRITE(TX020);
TX020= 'PLAN,TEAM,CCA ID,USER ID,CASH TOTAL,FOLDER NAME,TRANS TOTAL,DIFFERENCE';
OCFILE9_WRITE(TX020);
GOBACK;
*