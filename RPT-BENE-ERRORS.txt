****************************************************************************************************
**
**    TEXT FILE NAME: RPT-BENE-ERRORS (external)
**
**    DESCRIPTION: Create a report of all participants with beneficiaries in a priority
**                            where the total percentage <> 100%
**
**
**    CREATED BY                  DATE          WMS
**    ================= ========  ========
**    Gary Pieratt                10/14/2010     4744
**
**    MODIFICATION HISTORY:
**
**    PROGRAMMER                   DATE      WMS   Reason
**    ========================  ==========  =====  =================================================
**
****************************************************************************************************
SD080=9999999; WK007=0; TX003=''; TX004='';
IF OCFILE1_OPEN(NAME:(OCTEXT_GETENV('XJOB')+'/'+OCTEXT_GETENV('EBSRPTPFX')+'RPT-BENE-ERRORS.txt') MODE:'OUTPUT');
OCFILE1_WRITE((OCTEXT_REPLICATE(' ' 40)+'Beneficiary Errors Report'));
OCFILE1_WRITE((OCTEXT_REPLICATE(' ' 47)+OCFMT_DATE4(OCDATE_CURRENT())));
OCFILE1_WRITE(' ');
OCDATA_CODESTROY(NAME:'BENE-PP-DATA');
OCDATA_COCREATE(NAME:'BENE-PP-DATA' TYPE:'O' KEYLENG:34 TX10S:1 TX40S:2 NUMS:2);
PLPLOBJ_VIEW(PLANLO:'A00001');
LOOP WHILE PLPLOBJ_NEXT();
  TX001=PLPLOBJ_DE(011);
  PTPHOBJ_VIEW(PLAN:TX001);
  LOOP WHILE PTPHOBJ_NEXT();
    TX002=PTPHOBJ_DE(007); WK001=0; WK002=0; WK003=0; WK004=0; WK005=0; WK006=0;
    PTAIOBJ_VIEW(PLAN:TX001 PARTID:TX002);
    LOOP WHILE PTAIOBJ_NEXT();
      IF PTAIOBJ_DE(100) = '1';
         WK001 = 1;
         WK004=+(PTAIOBJ_NUMDE(230) * 100);
         OCDATA_ITEMINIT(NAME:'BENE-PP-DATA');
         OCDATA_DESET(DENUM:500 VALUE:'1');
         OCDATA_DESET(DENUM:600 VALUE:PTAIOBJ_DE(240));
         OCDATA_DESET(DENUM:601 VALUE:PLPLOBJ_DE(951));
         OCDATA_DESET(DENUM:1   VALUE:(PTAIOBJ_NUMDE(230) * 100));
         OCDATA_DESET(DENUM:2   VALUE:PTAIOBJ_NUMDE(008));
         OCDATA_ITEMADD(NAME:'BENE-PP-DATA' KEY:(('1'+PTAIOBJ_DE(240)+OCFMT(PTAIOBJ_NUMDE(008) 'Z3'))));
      ELSE;
         IF PTAIOBJ_DE(100) = '2';
            WK002 = 1;
            WK005=+(PTAIOBJ_NUMDE(230) * 100);
            OCDATA_ITEMINIT(NAME:'BENE-PP-DATA');
            OCDATA_DESET(DENUM:500 VALUE:'2');
            OCDATA_DESET(DENUM:600 VALUE:PTAIOBJ_DE(240));
            OCDATA_DESET(DENUM:601 VALUE:PLPLOBJ_DE(951));
            OCDATA_DESET(DENUM:1   VALUE:(PTAIOBJ_NUMDE(230) * 100));
            OCDATA_DESET(DENUM:2    VALUE:PTAIOBJ_NUMDE(008));
            OCDATA_ITEMADD(NAME:'BENE-PP-DATA' KEY:(('2'+PTAIOBJ_DE(240)+OCFMT(PTAIOBJ_NUMDE(008) 'Z3'))));
         ELSE;
            IF PTAIOBJ_DE(100) = '3';
               WK003 = 1;
               WK006=+(PTAIOBJ_NUMDE(230) * 100);
               OCDATA_ITEMINIT(NAME:'BENE-PP-DATA');
               OCDATA_DESET(DENUM:500 VALUE:'3');
               OCDATA_DESET(DENUM:600 VALUE:PTAIOBJ_DE(240));
               OCDATA_DESET(DENUM:601 VALUE:PLPLOBJ_DE(951));
               OCDATA_DESET(DENUM:2    VALUE:PTAIOBJ_NUMDE(008));
               OCDATA_ITEMADD(NAME:'BENE-PP-DATA' KEY:(('3'+PTAIOBJ_DE(240)+OCFMT(PTAIOBJ_NUMDE(008) 'Z3'))));
            END;
         END;
      END;
    ENDLOOP;
    OCDATA_ITEMVIEW(NAME:'BENE-PP-DATA');
    LOOP WHILE OCDATA_ITEMNEXT(NAME:'BENE-PP-DATA');
      IF (OCDATA_DE(500) = '1') AND (WK004 <> 100);
         IF TX001 <> TX003;
            PERFORM 'PLAN-HEADER';
            TX003 = TX001;
         END;
         IF TX002 <> TX004;
            PERFORM 'PART-HEADER';
            TX004 = TX002;
         END;
         OCFILE1_WRITE((OCDATA_DE(600)+'    Primary           '+
                 OCFMT(OCDATA_NUMDE(1) 'F3.2')+'%    '+OCDATA_DE(601)));
         WK007 = 1;
      ELSE;
         IF (OCDATA_DE(500) = '2') AND (WK005 <> 100);
            IF TX001 <> TX003;
               PERFORM 'PLAN-HEADER';
               TX003 = TX001;
            END;
            IF TX002 <> TX004;
               PERFORM 'PART-HEADER';
               TX004 = TX002;
            END;
            OCFILE1_WRITE((OCDATA_DE(600)+'    Secondary         '+
                    OCFMT(OCDATA_NUMDE(1) 'F3.2')+'%    '+OCDATA_DE(601)));
            WK007 = 1;
         ELSE;
            IF (OCDATA_DE(500) = '3') AND (WK006 <> 100);
               IF TX001 <> TX003;
                  PERFORM 'PLAN-HEADER';
                  TX003 = TX001;
               END;
               IF TX002 <> TX004;
                  PERFORM 'PART-HEADER';
                  TX004 = TX002;
               END;
               OCFILE1_WRITE((OCDATA_DE(600)+'    Tertiary          '+
                       OCFMT(OCDATA_NUMDE(1) 'F3.2')+'%    '+OCDATA_DE(601)));
               WK007 = 1;
            END;
         END;
      END;
    ENDLOOP;
    OCDATA_COEMPTY(NAME:'BENE-PP-DATA');
  ENDLOOP;
ENDLOOP;
IF WK007 = 0;
   TX001=' ';
   PERFORM 'PLAN-HEADER';
   OCFILE1_WRITE('Nothing to report.');
END;
OCFILE1_CLOSE();
OCDATA_CODESTROY(NAME:'BENE-PP-DATA');
END;
*
ROUTINE 'PLAN-HEADER';
IF TX003 <> '';
   OCFILE1_WRITE(' ');
   OCFILE1_WRITE(' ');
END;
OCFILE1_WRITE(('Plan: '+TX001));
GOBACK;
*
ROUTINE 'PART-HEADER';
OCFILE1_WRITE(' ');
OCFILE1_WRITE(('Participant: ***-**-'+OCSUB(TX002 6 4)+' - '+PTPHOBJ_DE(011)));
OCFILE1_WRITE(' ');
OCFILE1_WRITE('Beneficiary Name                  Priority         Percentage  Consultant');
GOBACK;