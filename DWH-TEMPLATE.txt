*
*
Brent Changes
*
********************************************************************************
**
**    TEXT FILE NAME:  --- DWH-TEMPLATE
**
**    WORK FIELDS                                 TEXT FIELDS
**    _____________________________________       _____________________________________________
**
**    WK001=                                      TX001=PLANLIST FILENAME ($PLANLISTFILE)
**    WK002=                                      TX002=PLANLIST RECORD STRING
**    WK003=                                      TX003=PLAN
**    WK004=                                      TX004=OUTPUT FILENAME ($DWHOUT)
**    WK005=OUT EXT FILE WRITE STATUS CONDITION   TX005=FOLDER_NAME
**    WK006=                                      TX006=WAIVE_IND
**    WK007=                                      TX007=TRANS_STOP_DT
**    WK008=                                      TX008=ASSESSMENT_FREQ
**    WK009=                                      TX009=OUTPUT RECORD STRING
**    WK010=VT610                                 TX010=
**    WK011=                                      TX011=
**    WK012=                                      TX012=
**    WK013=Run date - 1 yr                       TX013=
**    WK014=                                      TX014=
**    WK015=                                      TX015=FOLDER_NAME   ATTRIBUTE NAME
**    WK016=                                      TX016=WAIVE_IND     ATTRIBUTE NAME
**    WK017=                                      TX017=TRANS_STOP_DT ATTRIBUTE NAME
**    WK018=                                      TX018=ASSESSMENT_FREQ ATTRIBUTE NAME
**    WK019=                                      TX019=PLAN          ATTRIBUTE NAME
**    WK020=                                      TX020=
**    WK021=                                      TX021=
**    WK022= # TRANSACTIONS QUALIFIED             TX022= 
**    WK099= REPORT LINE COUNTER                  TX023= 
**
**
**    CREATED BY                           DATE         SMR
**    ==============================     ===========   ========
**    JUDY STEWART                       12/13/2011    5501 - Fees
**
**
**    MODIFICATION HISTORY:
**
**    PROGRAMMER                           DATE         SMR
**    ==============================     ========     ========
**    Rick Sica                          10/20/2014   11030     Optimize
********************************************************************************
TX015='FOLDER_NAME';   /* ATTRIBUTE NAMES */
TX016='WAIVE_IND';
TX017='TRANS_STOP_DT';
TX018='ASSESSMENT_FREQ';
TX019='PLPL011';
SD080=999999999;       /* MAX LOOPS */
TX004=OCTEXT_GETENV('DWHOUT');
IF OCFILV2_OPEN(NAME:TX004 MODE:'OUTPUT') = 0;
   RPLOG_WARN(MSG:('AUL - DWH-TEMPLATE.txt - DWHOUT does not exist/creating : '+TX004));
END;
TX001=OCTEXT_GETENV('PLANLISTFILE');
IF OCFILE1_OPEN(NAME:TX001 MODE:'INPUT')=0;
  RPLOG_WARN(MSG:('AUL - DWH-TEMPLATE.txt - PLANLISTFILE does not exist/exiting : ' + TX001));
ELSE;
  WK013=OCDATE_BUMP(SD003 YEAR:-1);
  LOOP WHILE OCFILE1_READ(INTO:TX002);                 /* READ FILE LOOP */
    OCCSV_SETLINE(TX002);
    OCCSV_VIEW();
    TX003=OCCSV_FIELD(1);                              /* PLAN */
    IF (TX003<>' ');                                   /* PLAN NOT BLANK */
      IF PLPLOBJ_GET(PLAN:TX003)>0;
        WK014=PLPLOBJ_NUMDE(804);
        IF (PLPLOBJ_DE(621)='0') OR (WK014=0) OR (WK014>=WK013);
          VTTDOBJ_VIEW(PLAN:TX003 FILEID:'       PERIODIC.PROP/T');     /* BROWSE FOLDER FOR TEMPLATES */
          LOOP WHILE VTTDOBJ_NEXT();            
            IF (VTTDOBJ_NUMDE(500)=1) AND (VTTDOBJ_DE(070)='000000000');  /* ONLY ACTIVE & PLAN LEVEL TRXNS */
              TX005=VTTDOBJ_DE(956);                    /* FOLDER_NAME */
              IF OCSUB(TX005 1 3) = 'FEE';    /* ONLY FEE TEMPLATE FOLDERS */
                IF VTFHOBJ_GET(PLAN:TX003 FILEID:TX005);
                   TX099=TX015+SDTAB+TX005+SDTAB+TX019+SDTAB+TX003+SDTAB+TX015+SDTAB+TX005;
                   OCFILV2_WRITE(TX099);                /* WRITE EXTRACT FILE RECORD */
                   TX006=VTTDOBJ_DE(581);               /* WAIVE_IND - do not send if blank */
                   IF TX006<>'';
                      TX099=TX016+SDTAB+TX006+SDTAB+TX019+SDTAB+TX003+SDTAB+TX015+SDTAB+TX005;
                      OCFILV2_WRITE(TX099);             /* WRITE EXTRACT FILE RECORD */
                   END;
                   WK001=0;                             /* TRANS_STOP_DT */
                   WK010=VTTDOBJ_NUMDE(610);
                   WK095=VTFHOBJ_NUMDE(595);
                   IF (WK010<>0);          
                      IF (WK095<>0);       
                        IF (WK010<=WK095);
                           WK001=WK010;
                        ELSE;
                           WK001=WK095;
                        END;
                      ELSE;
                        WK001=WK010;
                      END;
                   ELSE;
                      WK001=WK095; 
                   END;
                   IF WK001<>0;
                     TX007=OCFMT_DATE3(WK001);
                     TX099=TX017+SDTAB+TX007+SDTAB+TX019+SDTAB+TX003+SDTAB+TX015+SDTAB+TX005;
                     OCFILV2_WRITE(TX099);
                   END;
                   TX008=VTFHOBJ_DE(550);               /* ASSESSMENT_FREQ - always send */
                   TX099=TX018+SDTAB+TX008+SDTAB+TX019+SDTAB+TX003+SDTAB+TX015+SDTAB+TX005;
                   OCFILV2_WRITE(TX099);
                END;
              END;
            END;
          ENDLOOP;                                       /* VTRAN LOOP END */
        END;
      END;
    END;                                               /* PLAN NOT BLANK END */
  ENDLOOP;                                             /* READ FILE LOOP END */
END;                                                   /* FILE OPEN OKAY */
OCFILE1_CLOSE();
OCFILV2_CLOSE();
*******************************************************************************************************
**
** SUBROUTINES
**
*******************************************************************************************************
**
*******************************************************************************************************



