********************************************************************************
**
**    TEXT FILE NAME:  --- ONE-LOAN-PRECONV.TXT
**
**    WORK FIELDS                                 TEXT FIELDS
**    _____________________________________       _____________________________________________
**
**    WK001=INV IC STATUS CONDITION               TX001=OUTPUT FILENAME
**    WK002=                                      TX002=OUTPUT RECORD STRING
**    WK003=UPDATE PL751                          TX003=PLAN
**    WK004=PLAN LN FUND BALANCE                  TX004=NEW PL751 VALUE '1'
**    WK005=OUT EXT FILE WRITE STATUS CONDITION   TX005=OLD PL751 VALUE
**    WK006=                                      TX006=FORMATTED TIME STRING
**    WK007=                                      TX007=
**    WK008=                                      TX008=
**    WK009=                                      TX009=
**    WK010=                                      TX010=
**    WK011=                                      TX011=
**    WK012=                                      TX012=
**    WK013=                                      TX013=
**    WK014=                                      TX014=
**    WK015=                                      TX015=
**    WK016=                                      TX016=
**    WK017=                                      TX017=
**    WK018=                                      TX018=
**    WK019=                                      TX019=
**    WK020=                                      TX020=
**    WK021=                                      TX021= ENVIRONMENT PREFIX TEXT HEADER
**    WK022= # TRANSACTIONS QUALIFIED             TX022= (R)EPORT OR (U)PDATE FLAG
**    WK099= REPORT LINE COUNTER                  TX023= REPORT or UPDATE TEXT HEADER
**
**
**    CREATED BY                           DATE         SMR
**    ==============================     ===========   ========
**    JUDY STEWART                      05/7/2007     CC11758
**
**
**    MODIFICATION HISTORY:
**
**    PROGRAMMER                           DATE         SMR
**    ==============================     ========     ========
**
********************************************************************************
**
** INITIALIZATIONS
**
SD080=9999999;
TX022="U";                   /* R = REPORT ONLY   U = UPDATE & REPORT */
WK022=0;                     
WK099=0;
TX004='1';  /* PL751 UPDATE VALUE */
**
** REPORT/UPDATE variables
**
IF TX022='R';
  TX023='REPORT';
ELSEIF TX022='U';
  TX023='UPDATE';
END;
TX021=OCTEXT_GETENV('EBSRPTPFX'); /* ENVIRONMENT PREFIX */
**
** REPORT DEFINITION
**
OCPAGE_SETUP(RPTSEQ:991 PLAN:SD001); /* PL751 UPDATE REPORT */
**
********************************************************************************
** REPORT/UPDATE LOGIC 
**
**     1)   PL751
**
**          Excel (CSV) spreadsheet to be used to capture value of PL751 prior
**          to update
**
**            Read through ALL PLANS
**             If plan has IC / FC match LN and a balance and PL751<>1
**               >write plan & pl751 value to a file
**               >Update plan with PL751='1'
**
********************************************************************************
TX006=OCFMT(SD004 'Z6');                          /* CURRENT TIME */
**TX001=OCTEXT_GETENV('POSTOUT') + "/PL751_PRECONV_"+OCTEXT_TRIM(TX006)+".csv";
TX001=OCTEXT_GETENV('POSTOUT') + "/PL751_PRECONV.csv";
PERFORM 'CREATE.FILE';                            /* CREATE OUTPUT FILE */
**
**
**PLPLOBJ_VIEW(PLANLO:'A' PLANHI:'C');              /* P6JLS UNIT TESTING */
PLPLOBJ_VIEW(PLANLO:'A');
LOOP WHILE PLPLOBJ_NEXT();
   TX003 = PLPLOBJ_DE(DENUM:011);
   TX005=PLPLOBJ_DE(751);                         /* PRIOR PL751 VALUE */
   IF TX005<>'1';
**      OCSHOW_LINE('Examining Plan# ' TX003 ' for LN fund.');
      PERFORM 'SCAN.INVESTMENTS';                 /* DOES PLAN HAVE LN INVESTMENT */
      IF WK001>0;
         PERFORM 'PL751.DETAIL';                  /* PRINT ON REPORT */
         PERFORM 'PL751.FILE';                    /* WRITE TO FILE */
         IF TX022='U';                            /* IF UPDATE */
           PLPLOBJ_SETDE(DENUM:751 VALUE:TX004);  /* UPDATE PL751 TO LOANS ALLOWED */
           PLPLOBJ_UPDATE();                              
         END;
      END;                                        /* PLAN HAS A BALANCE IN LN FUND */
   ELSE;
**   OCSHOW_LINE('Examining Plan# ' TX003 '>> PL751 = 1.');
   END;                                           /* PL751 NOT ALREADY 1 */
ENDLOOP;                                          /* PLAN LOOP */
**
** WRITE COUNT OF TOTAL TRANSACTIONS
**
PERFORM 'PL751.TOTALS';
**
** OUTPUT PAGE IN BUFFER
**
IF WK099 > 0;
  OCPAGE_WRITE(RPTSEQ:991);
END;
**
OCSHOW_LINE('OMNI 5.8 LOAN PRE CONVERSION <' TX023 '> FINISHED.');
**
*******************************************************************************************************
**
** SUBROUTINES
**
**************************************************************************************
ROUTINE 'CREATE.FILE'; 
**************************************************************************************
IF OCFILV1_OPEN(NAME:TX001 TRAILSPACES: 'N' MODE:'OUTPUT')=0; /* Create output file */
  RPLOG_ERROR(MSG:('ONE-LOAN-PRECONV : COULD NOT CREATE OUTPUT EXTRACT FILE - ' + TX001));
END;
*******************************************************************************************************
ROUTINE 'PL751.FILE';
*******************************************************************************************************
TX002='"'+TX003+'"'+","+'"'+TX005+'"';
IF TX002<>'';
  WK005=OCFILV1_WRITE(TX002);
  IF WK005=0;
    RPLOG_ERROR(MSG:('ONE-LOAN-PRECONV: COULD NOT WRITE TO OUTPUT EXTRACT FILE '+TX001));
  ELSE;
    TX002='';
  END;
END;
*******************************************************************************************************
ROUTINE 'SCAN.INVESTMENTS';
*******************************************************************************************************
WK001=IVICOBJ_GET(PLAN:TX003 FUNDIV:'LN');
IF WK001=1;
  BARUN_PLSTART(PLAN:TX003);
  WK004=FC999@LN*;
  IF FC999@LN*>0;
    WK003=1;
    OCSHOW_LINE('Plan# ' TX003 ' has LN fund with  $' OCTEXT_TRIM(OCFMT_CASH(WK004)) ' balance .');
  ELSE;
    OCSHOW_LINE('Plan# ' TX003 ' has LN fund with a ' OCTEXT_TRIM(OCFMT_CASH(WK004)) ' balance .');
    WK003=0;
  END;
  BARUN_PLEND();
END;
*******************************************************************************************************
ROUTINE 'PL751.DETAIL';
*******************************************************************************************************
  IF (WK099 > 62) OR (WK099 = 0);
    IF WK099 > 62;
      OCPAGE_WRITE(RPTSEQ:991);
      PERFORM 'PL751.HEADER';
    END;
    IF WK099 = 0;
      PERFORM 'PL751.HEADER';
    END;
  END;
  OCPAGE_SET(TX003                   LINE:WK099 COL:001);
  OCPAGE_SET(TX005                   LINE:WK099 COL:025);
  OCPAGE_SET(TX004                   LINE:WK099 COL:045);
  WK099=+1;
  WK022=+1;
**
*******************************************************************************************************
ROUTINE 'PL751.HEADER';
*******************************************************************************************************
  OCPAGE_SET('RUN DATE : '           LINE:001 COL:001);
  OCPAGE_SET(OCFMT_DATE3(SD003)      LINE:001 COL:012);
  OCPAGE_SET('ENV/TYPE : '           LINE:001 COL:060);
  OCPAGE_SET(TX021                   LINE:001 COL:070);
  OCPAGE_SET('/'                     LINE:001 COL:073);
  OCPAGE_SET(TX023                   LINE:001 COL:074);
  OCPAGE_SET(' OMNI 5.8 PRE LOAN CONVERSION - STORE PL751 IN PL751_PRECONV.csv' LINE:002 COL:005);
  OCPAGE_SET('PLAN'                  LINE:004 COL:001);
  OCPAGE_SET('PL751-STORED'          LINE:004 COL:021);
  OCPAGE_SET('PL751-TEMP'            LINE:004 COL:041);
  WK099=5;
**
*******************************************************************************************************
ROUTINE 'PL751.TOTALS';
*******************************************************************************************************
WK099=+2;
IF TX022='U';
  OCPAGE_SET('TOTAL NUMBER OF PLANS UPDATED: '   LINE:WK099  COL:002);
ELSEIF TX022='R';
  OCPAGE_SET('TOTAL NUMBER OF PLANS REPORTED : ' LINE:WK099  COL:002);
END;
  OCPAGE_SET(OCFMT(WK022 'INTEGER5')             LINE:WK099  COL:034);
WK099=+2;
OCPAGE_SET('OUTPUT EXTRACT FILE UNIX LOCATION : ' LINE:WK099 COL:001);
OCPAGE_SET(TX001                                  LINE:WK099 COL:037);
WK099=+1;
**
*******************************************************************************************************
** END
*******************************************************************************************************

