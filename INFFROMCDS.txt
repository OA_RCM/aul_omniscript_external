********************************************************************************
**
**    Text File Name:  --- INFFROMCDS: This calculator has been developed to read
**    the check information from CDS. The input data will be used to update the
**    Check-Num, Check-Status and Check-Date fields in the Disbursement Activity records.
**    To update the correct records the Omni file must be read using the following key values:
**
**               1. Plan Number
**               2. Participant Number
**               3. Trade Date
**               4. Run Date
**               5. Run Time
**               6. Sequence Number
**
**    This combination of fields will allow us to narrow the transaction down to unique record.
**    The input file is used to drive the update process, so the file is read thru only once and
**    a matching Disbursement Activity record is retreived.
**
**    Input File: cdstfile.txt
**
**                Plan-No      1 -  6
**                Filler       7 - 14
**                Ref-No      15 - 26
**                Part-No     27 - 38
**                Check-Amt   39 - 49
**                Filler      50 - 52
**                Check-No    53 - 59
**                Check-Date  60 - 65
**
**
**    Output File: checkerr.txt - formated report 'Disbursement Activity Check Data Reject Report'.
**
**
**    Work Fields                                 Text Fields
**    _____________________________________       _____________________________________________
**
**    WK001=                                      TX001=
**    WK002=                                      TX002=
**    WK003=                                      TX003=
**    WK004=                                      TX004=
**    WK005=                                      TX005=
**    WK006=                                      TX006=
**    WK007=                                      TX007=
**    WK008=                                      TX008=
**    WK009=                                      TX009=
**    WK010=                                      TX010=
**    WK011=                                      TX011=
**    WK012=                                      TX012=
**    WK013=                                      TX013=
**    WK014=                                      TX014=
**    WK015=                                      TX015=
**    WK016=                                      TX016=
**    WK017=                                      TX017=
**    WK018=                                      TX018=
**    WK019=                                      TX019=
**    WK020=                                      TX020=
**    WK021=                                      TX021=
**    WK022=                                      TX022=
**
**
**
**    Created by                           Date         SMR
**    ==============================     ===========   ========
**    Danny Hagans                       02/15/2001     pens3258
**
**
**    Modification History:
**
**    Programmer                           Date         SMR
**    ==============================     ========     ========
**    Danny Hagans                       09/18/2002    CC3309
**    Danny Hagans                       04/02/2004    CC4636
**    Danny Hagans                       02/28/2006    CC9033
**    Danny Hagans                       03/30/2006    CC9982
**    Judy Stewart                       03/09/2007    CC11251
********************************************************************************
WK001=0;
TX003='Processed - No Errors';
WK005=1;
TX031='N';
WK021=0;
WK020=0;
SD080=9999999;
* Set input/output file name and location
TX001=OCTEXT_GETENV('XJOB') + '/cdstfile.txt';
*TX001='/home/CDXNET/cdstfile.txt';
OCSHOW(TX001);
TX090=OCTEXT_GETENV('XJOB') + '/INF1090E';
OCSHOW(TX090);
* Open CDS input file
IF OCFILE1_OPEN(NAME:TX001 MODE:'INPUT')=0;
   TX003='OPEN ERROR INPUT ' + TX001;
END;
* Open CDS output - update status file
IF OCFILE2_OPEN(NAME:TX090 MODE:'OUTPUT')=0;
   TX003='OPEN ERROR OUTPUT ' + TX090;
END;
* Read first record of input file
TX002=OCFILE1_READ();
IF SD100=1;
   TX003="CDSIN READ FAILED" ;
END;
* Read input file until EOF
LOOP WHILE (SD100=0);
     TX004=OCTEXT_SUB(TX002 1   6);              /* Plan Number        */
     TX005=OCTEXT_SUB(TX002 15 12);              /* Reference Number   */
     SD040=OCTEXT_SUB(TX005 1 6);
     WK012=SD040;                                /* Check date is converted to a date format */
     TX006=OCTEXT_SUB(TX005 1 3) + '-' + OCTEXT_SUB(TX005 4 2) + OCTEXT_SUB(TX002 33 6);            /* Participant Number */
     TX007=OCTEXT_SUB(TX002 39 11);              /* Check Amount       */
     TX008=OCTEXT_SUB(TX002 53 7);               /* Check Number       */
     TX021=OCTEXT_SUB(TX005 6 6);                /* CDS Run Date       */
     SD040=TX021;
     WK013=SD040;                                /* Run Date converted to numeric format for key search */
     TX022=OCTEXT_SUB(TX005 1 5);                /* First 5 digits of soc-sec-no  */
     TX030=OCTEXT_SUB(TX005 12 1);               /* Sequence Number     */
     WK014=OCTEXT_TONUM(TX030);                  /* Sequence number is converted to numeric to check against a key value */
     WK002=OCTEXT_TONUM(TX007);                  /* Check Amount is converted to a numeric to */
     WK009=TX008;
     WK002=WK002/100;
     TX007=OCFMT(CASH:WK002);
     TX009=OCTEXT_SUB(TX022 1 3) + OCTEXT_SUB(TX022 4 2) + OCTEXT_SUB(TX002 34 4); /* Participant No */
     TX029=OCTEXT_SUB(TX002 60 6);
     SD040=TX029;
     WK022=SD040;
     TX010=OCTEXT_SUB(TX002 66 21);
     WK007=WK007+WK002;
     TX030='read ' + TX004 + ' ' +TX009 + ' ' + OCFMT(INTEGER5:WK013) + ' ' + OCFMT(INTEGER5:WK014);
     OCSHOW(TX030);
     PLPLOBJ_VIEW(PLAN:TX004);
     LOOP WHILE PLPLOBJ_NEXT();
        TX040=PLPLOBJ_DE(DENUM:025);
        TX043=PLPLOBJ_DE(DENUM:950);
        PMPMOBJ_VIEW(PLAN:TX004 PRODUCTID:TX040);
        LOOP WHILE PMPMOBJ_NEXT();
           TX041=PMPMOBJ_DE(DENUM:492);
           TX042=PMPMOBJ_DE(DENUM:727);
        ENDLOOP;
     ENDLOOP;
     DBDBOBJ_VIEW(PLAN:TX004 PARTID:TX009 RUNDATE:WK013);
     LOOP WHILE DBDBOBJ_NEXT();
          WK015=DBDBOBJ_NUMDE(DENUM:011);
          TX030=(OCFMT(INTEGER5:DBDBOBJ_NUMDE(DENUM:413))) + ' = ' + OCFMT(INTEGER5:WK014) + ' ' + DBDBOBJ_DE(DENUM:351) + ' ' + OCFMT(INTEGER5:WK015);
*          OCSHOW(TX030);
*          IF (DBDBOBJ_NUMDE(DENUM:413)=WK014);
              WK008=2;
** CC11251 - REMOVE OVERRIDE AMOUNTS from net distribution amount calculation
**           OMNI 5.5 appears to store tax amounts (when overridden) in both locations (regular tax field and the override tax field.)
**           Prior to 5.5, if tax amounts were overridden, they were only stored in the override tax field locations.  Now they are stored in both places.
              WK024=DBDBOBJ_NUMDE(250) + DBDBOBJ_NUMDE(270) + DBDBOBJ_NUMDE(283) + DBDBOBJ_NUMDE(320) + DBDBOBJ_NUMDE(330)  ;
              WK025=DBDBOBJ_NUMDE(200) - WK024 - DBDBOBJ_NUMDE(674) - DBDBOBJ_NUMDE(470);  /* cc11251 - remove loan defaults from check net amt calc */
              TX030 = 'calc ' + TX009 + ' CDS= ' + OCFMT(CASH:WK02) + ' DIST= '  + OCFMT(CASH:WK025);
              OCSHOW(TX030);
*              IF (DBDBOBJ_NUMDE(DENUM:351) <= 0);
               IF (OCFMT(CASH:WK002)=OCFMT(CASH:WK025)); /* Match check amt to net amout from DBDBOBJ */
                 DBDBOBJ_SETDE(DENUM:351 VALUE:WK009);
                 DBDBOBJ_SETDE(DENUM:352 VALUE:WK008);
                 DBDBOBJ_SETDE(DENUM:353 VALUE:WK022);
                 DBDBOBJ_UPDATE();
                 WK004=+1;
                 TX020=DBDBOBJ_ERRMSG();
                 TX013=DBDBOBJ_DE(DENUM:008);
                 TX014=DBDBOBJ_DE(DENUM:351);
                 TX015=DBDBOBJ_DE(DENUM:352);
                 SD040=DBDBOBJ_DE(DENUM:353);
                 WK006=DBDBOBJ_NUMDE(DENUM:200);
                 TX023=DBDBOBJ_NUMDE(DENUM:009);
                 TX024=DBDBOBJ_NUMDE(DENUM:010);
                 TX025=DBDBOBJ_NUMDE(DENUM:011);
                 TX030=TX041 + TX004 + TX042 + TX043 + '  ' + TX005 + ' ' + TX006 + ' ' + TX007 + ' ' + TX008 + ' ' + OCDATE_MMDDYYYY(WK022) + '  UPDATE ';
                 OCFILE2_WRITE(TX030);
*                 OCSHOW(TX030);
                 WK005=+1;
                 WK021=+1;
                 WK018=1;
               END;
*          END;
          TX011=' ';
          TX012=' ';
          TX013=' ';
          WK003=0;
          WK006=0;
     ENDLOOP;
* If matching tranaction record not found
     IF WK018=0;
        TX030=TX041 + TX004 + TX042 + TX043 + '  ' + TX005 + ' ' + TX006 + ' ' + TX007 + ' ' + TX008 + ' ' + OCDATE_MMDDYYYY(WK022) + "  REJECT *** ";
        OCFILE2_WRITE(TX030);
*        OCSHOW(TX030);
        WK005=+1;
     END;
     WK018=0;
     WK001=+1;
     TX002=OCFILE1_READ();
ENDLOOP;
*
OCFILE1_CLOSE();
*
WK020=WK001-WK021;              /* Total records read minus updated records giving rejected records */
* Write process end totals
TX030=' ';
*OCFILE2_WRITE(TX030);
TX030='Total Records Processed = ' + OCFMT(INTEGER5:WK001);
*OCFILE2_WRITE(TX030);
OCSHOW(TX030);
TX030='Total Records Updated   = ' + OCFMT(INTEGER5:WK021);
*OCFILE2_WRITE(TX030);
OCSHOW(TX030);
TX030='Total Records Rejected  = ' + OCFMT(INTEGER5:WK020);
*OCFILE2_WRITE(TX030);
OCSHOW(TX030);
OCFILE2_CLOSE();
OCFILE2_SORT(NAME:TX090);
*
