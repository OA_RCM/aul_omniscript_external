********************************************************************************
*  PRONVEST.EXTRACT                                                            *
*  JOBCALC OmniScript to extract ProNvest data                                 *
*  Store List of Plans to Include In PRONVEST.PLANLIST Textfile in Plan 000001 *
*  Configure UDFS with PRONVEST.CTL in each plan                               *
*  WRITES 3 Tab Delineated FILES TO DATA DIRECTORY                             *
*         (Plan, Investment, Participant Data)                                 *
*  --- FIELD DEFINITIONS                                                       *
*  TX001-TX003 (FILE NAMES)                                                    *
*  TX005 - CONTROL FILE NAME                                                   *
*  TX006 - MATCH SOURCE FLAG                                                   *
*  TX007 - COMPANY STOCK FLAG                                                  *
*  TX008 - Plan List                                                           *
*  TX009 - QDIA Flag PM756   WMS4488
*  TX040 - Defaulted Election Flag PH634
*  WK001 - LOAN BALANCE                                                        *
*  WK002 - Number of Plans Reported                                            *
*  WK030-WK039 - PH data elements (From Control File)                          *
*  TX030-TX039 - PH Data Element Values                                        *
*  ---- Modification History                                                   *
*  12/08/2003 - ebsdab - wrote program                                         *
*  07/22/2004 - ebssmb - updated script to add contribution info from previous *
*                        month as well as FC063 data                           *
*                                                                              *
*  08/16/2005 - Brent  - Updated script for AUL                                *
*  11/16/2006 - Glen   - Added FC062 and SC220                                 *
*  01/29/2008 - Kim    - Added IC300                                           *
*  04/20/2009 - Judy   - Omniplus 5.8 Upgrade                                  *
*  11/04/2010 - Rick   - w4454 - Plan Locking add PLEND
*  06/14/2011 - Louis  - WMS04488 - RIA -QDIA
********************************************************************************
* File names
   TX001 = "$EBSXFER/pronvest/out/PRONVEST.PLAN.TXT";
   TX002 = "$EBSXFER/pronvest/out/PRONVEST.INVEST.TXT";
   TX003 = "$EBSXFER/pronvest/out/PRONVEST.PART.TXT";
   TX004 = "$EBSXFER/pronvest/out/PRONVEST.FUND.TXT";
*
* Create/Initialize Extract File(s)
OcFilv1_Open(Name:TX001 Mode:"OUTPUT");
OcFilv1_Write(("EXTRACT FILE CREATED " + OCTEXT_DATETIME()));
OcFilv1_Close();
OcFilv1_Open(Name:TX002 Mode:"OUTPUT");
OcFilv1_Write(("EXTRACT FILE CREATED " + OCTEXT_DATETIME()));
OcFilv1_Close();
OcFilv1_Open(Name:TX003 Mode:"OUTPUT");
OcFilv1_Write(("EXTRACT FILE CREATED " + OCTEXT_DATETIME()));
OcFilv1_Close();
OcFilv1_Open(Name:TX004 Mode:"OUTPUT");
OcFilv1_Write(("EXTRACT FILE CREATED " + OCTEXT_DATETIME()));
OcFilv1_Close();
** setup OcCsv - TAB DELINEATED FILES
OcCsv_Reset();
OcCsv_SetDelimiter(SDTAB);
**
SdLoopMax=999999999;
TXTXOBJ_VIEW(PLan:"000001" Fileid:"PRONVEST.PLANLIST");
Loop While TXTXOBJ_NEXT();
   TX008 = OcSub(TXTXOBJ_Data() 1 6);
   If OcSub(TX008 1 1) <> "*";
      If PLPLOBJ_GET(Plan:TX008); /* duplicate PL read is intentional */
         WK002 =+ 1;
         BARUN_PLSTART(Plan:TX008);
         TX009='N';   
         IF PMPMOBJ_GET(PLAN:TX008 PRODUCTID:PL025)=1;
            IF PMPMOBJ_DE(756) = 'P';   /* WMS4488 */
               TX009='Y';
            END;            
         END;
         PERFORM "GET.CONTROL.FILE";
         PERFORM "EXTRACT.PLAN.INFO";
         PERFORM "EXTRACT.INVEST.INFO";
         PERFORM "EXTRACT.FUND.INFO";
         BARUN_PTVIEW(PLAN:PL011);
         OCFILV1_OPEN(Name:TX003 MODE:'EXTEND');
         LOOP WHILE BARUN_PTNEXT();
            PERFORM "EXTRACT.PART.INFO";
         ENDLOOP;
         OCFILV1_CLOSE();
         BARUN_PLEND();
      End;
   End;
EndLoop;
*
*-----------------------------------------------------------------------------*
*
Routine "GET.CONTROL.FILE";
   TX005 = "PRONVEST.CTL/" + PL011 + "/000001";
   OCCNTL_TableLoadCtl(Table:TX005);
   TX006 = "N";
   TX007 = "N";
   TX010 = ' ';
   Each_FC@***;
      IF SC007 = "D"; TX006 = "Y"; END;  /* MATCH FLAG */
*      IF FC080 = "0"; TX007 = "Y"; END;  /* COMPANY STOCK FLAG */
      IF IC080 = "0"; TX007 = "Y"; END;  /* WMS1519B - COMPANY STOCK FLAG */
      TX010=SC220;
   EndEach;
   WK030 = OcCntl_FieldValueN("PH-DENUM-PHONE");
   WK031 = OcCntl_FieldValueN("PH-DENUM-WORK-ADDRESS1");
   WK032 = OcCntl_FieldValueN("PH-DENUM-WORK-ADDRESS2");
   WK033 = OcCntl_FieldValueN("PH-DENUM-WORK-CITY");
   WK034 = OcCntl_FieldValueN("PH-DENUM-WORK-STATE");
   WK035 = OcCntl_FieldValueN("PH-DENUM-WORK-ZIP");
   WK036 = OcCntl_FieldValueN("PH-DENUM-WORK-PHONE");
   WK037 = OcCntl_FieldValueN("PH-DENUM-CONTRIB-DATE");
   WK038 = OcCntl_FieldValueN("PH-DENUM-CONTRIB-PERCENT");
   WK039 = OcCntl_FieldValueN("PH-DENUM-CONTRIB-FIXEDAMT");
GoBack;
*
Routine "EXTRACT.PLAN.INFO";
   OcFilv1_Open(Name:TX001 Mode:'EXTEND');
   OcCsv_Empty();
   OcCsv_Addfield(PL011
                  OcText_Flow(PL902 " " PL906)
                  PLPLOBJ_DEVALNAME(711)  /* PL711 LEGAL VALUE */
                  TX006
                  OcCntl_FieldValue("PL-MATCH-YN")
                  OcCntl_FieldValue("PL-MATCH-%")
                  OcCntl_FieldValue("PL-MATCH-WAIT-YN")
                  OcCntl_FieldValue("PL-MATCH-WAIT-PERIOD")
                  PL933
                  PL933
                  PL912
                  PL922
                  PL918
                  PL919
                  PL920
                  PL924
                  PL925
                  PL926
                  OcCntl_FieldValue("PL-COMPANY-CONTACT-NAME")
                  OcCntl_FieldValue("PL-COMPANY-CONTACT-PHONE")
                  OcCntl_FieldValue("PL-COMPANY-CONTACT-EMAIL")
                  OcCntl_FieldValue("PL-COST-TRADE-FUNDS-YN")
                  TX007
                  OcCntl_FieldValue("PL-STOCK-LIMIT")
                  OcCntl_FieldValue("PL-BROKER-FIRSTNAME")
                  OcCntl_FieldValue("PL-BROKER-LASTNAME")
                  OcCntl_FieldValue("PL-BROKER-ADDRESS1")
                  OcCntl_FieldValue("PL-BROKER-ADDRESS2")
                  OcCntl_FieldValue("PL-BROKER-CITY")
                  OcCntl_FieldValue("PL-BROKER-STATE")
                  OcCntl_FieldValue("PL-BROKER-ZIP")
                  OcCntl_FieldValue("PL-BROKER-PHONE")
                  OcCntl_FieldValue("PL-BROKER-EMAIL")
                  OcCntl_FieldValue("PL-PROVIDER-FIRSTNAME")
                  OcCntl_FieldValue("PL-PROVIDER-LASTNAME")
                  OcCntl_FieldValue("PL-PROVIDER-ADDRESS1")
                  OcCntl_FieldValue("PL-PROVIDER-ADDRESS2")
                  OcCntl_FieldValue("PL-PROVIDER-CITY")
                  OcCntl_FieldValue("PL-PROVIDER-STATE")
                  OcCntl_FieldValue("PL-PROVIDER-ZIP")
                  OcCntl_FieldValue("PL-PROVIDER-PHONE")
                  OcCntl_FieldValue("PL-PROVIDER-EMAIL")
                  OcCntl_FieldValue("PL-PROVIDER-URL")
                  TX010
                  TX009
                  );
   OcCsv_ToClip();
   OcFilv1_FromClip();
   OcFilv1_Close();
GoBack;
*-----------------------------------------------------------------------------*
Routine "EXTRACT.INVEST.INFO";
   OcFilv1_Open(Name:TX002 Mode:'EXTEND');
   Each_IC@***;
      OcCsv_Empty();
      OcCsv_Addfield(IC005
                     IC008
                     IC140
                     IC220
                     IC300
                     );
      OcCsv_ToClip();
      OcFilv1_FromClip();
   EndEach;
   OcFilv1_Close();
GoBack;
*-----------------------------------------------------------------------------*
Routine "EXTRACT.FUND.INFO";
   OcFilv1_Open(Name:TX004 Mode:'EXTEND');
   Each_FC@***;
      OcCsv_Empty();
      OcCsv_Addfield(FC022
                     FC026
                     FC063
                     FC062
                    );
      OcCsv_ToClip();
      OcFilv1_FromClip();
   EndEach;
   OcFilv1_Close();
GoBack;
*-----------------------------------------------------------------------------*
Routine "EXTRACT.PART.INFO";
   /* LOAN Balance */
   WK006 = 0;
   EACH@***;
      IF (FC060='2') OR (FC060='5'); /* WMS1519B */
         WK006 =+ PF999;
      END;
   ENDEACH;
   /* get control file DE's */
* - this code only works after 5.20 PTF 21, instead of the following 10 statements
*   OCTX_Clear(From:30 To:39);
   TX030 = "";
   TX031 = "";
   TX032 = "";
   TX033 = "";
   TX034 = "";
   TX035 = "";
   TX036 = "";
   TX037 = "";
   TX038 = "";
   TX039 = "";
   PTPHOBJ_GET(Plan:PH006 Partid:PH007);
   If PTPHOBJ_DEVALID(WK030); TX030=PTPHOBJ_DE(WK030);  End;
   If PTPHOBJ_DEVALID(WK031); TX031=PTPHOBJ_DE(WK031);  End;
   If PTPHOBJ_DEVALID(WK032); TX032=PTPHOBJ_DE(WK032);  End;
   If PTPHOBJ_DEVALID(WK033); TX033=PTPHOBJ_DE(WK033);  End;
   If PTPHOBJ_DEVALID(WK034); TX034=PTPHOBJ_DE(WK034);  End;
   If PTPHOBJ_DEVALID(WK035); TX035=PTPHOBJ_DE(WK035);  End;
   If PTPHOBJ_DEVALID(WK036); TX036=PTPHOBJ_DE(WK036);  End;
   If PTPHOBJ_DEVALID(WK037); TX037=PTPHOBJ_DE(WK037);  End;
   If PTPHOBJ_DEVALID(WK038); TX038=PTPHOBJ_DE(WK038);  End;
   If PTPHOBJ_DEVALID(WK039); TX039=PTPHOBJ_DE(WK039);  End;
   TX040=PTPHOBJ_DE(634); 
   KV1 = OCDATE_SET(OCDATE_BUMP(OCDATE_CURRENT() MONTH:-1) DAY:1);
   KV2 = OCDATE_MONTHEND(OCDATE_BUMP(OCDATE_CURRENT() MONTH:-1));
   /* ADD FIELDS */
   OcCsv_Empty();
   OcCsv_AddField(PH006
                  PH007
                  PH011
                  VR100
                  PH912
                  PH914
                  PH155
                  PH289
                  PH290
                  PH293
                  PH294
                  PH295
                  TX030
                  PH007
                  PH155
                  TX031
                  TX032
                  TX033
                  TX034
                  TX035
                  TX036
                  PH050
                  PH056
                  PH052
                  TX037
                  PH170
                  TX038
                  TX039
                  PH030
                  PF999@***
                  PF999@**D
                  PA200@***
                  TX040
                  );
   OcCsv_ToClip();
   OcFilv1_FromClip();
GoBack;