************************************************************************************
**
**    TEXT FILE NAME: INF-TRANS-UPD
**
**    Purpose: MidAmerica transaction automation.
**             Update userid's on Transactions.
**
**    Input: MidAmerica transaction input file
**
**    Ouput: OMNI transactions (T114, T381, T444,
**                              T301)
**
**    TEXT/WORK FIELDS
**    _____________________________________
**    TX011 = Plan
**    TX012 = Trans. type
**    TX013 = Trans. driver
**    WK014 = Order ID
**    TX015 = Investment ID
**    WK016 = Dollar Amt. from file
**    WK017 = Units
**    WK018 = Dollar Amt. with decimal
**    WK019 = Helper data (PM401)
**    TX018 = Investment ID
**    TX041 = T114 created
**    TX042 = T381 created
**    TX043 = T444 created
**    TX044 = Client ID
**    TX045 = File name (INPUT)
**    TX050 = Input record
**    TX051 = Prev. plan number
**
**
**    CREATED BY                           DATE         CC
**    ==============================     ===========   ========
**    Glen McPherson                      04/01/2008    11334
**
**    MODIFICATION HISTORY:
**
**    PROGRAMMER                           DATE         WMS
**    ==============================     ========     ========
**    Glen McPherson                      05/15/2009    2334
**
*************************************************************************************
SD080=999999999;
TX041 = 'N';TX042 = 'N';TX043 = 'N';TX052='N';
TX051 = ' ';
TX045 = OCTEXT_GETENV('INFILE');
OCSHOW_LINE('Input file=' TX045);
IF OCFILE1_OPEN(NAME:TX045 MODE:'INPUT')=1;
   LOOP WHILE OCFILE1_READ(INTO:TX050);
      OCCSV_SETLINE(TX050);
      OCCSV_VIEW();
      TX011=OCCSV_FIELD(1);  /* plan number */
      TX012=OCCSV_FIELD(2);  /* trans. type  */
      TX013=OCCSV_FIELD(3);  /* trans. driver */
      WK014=OCCSV_FIELD(4);  /* order id */
      TX015=OCCSV_FIELD(5);  /* investment id */
      WK016=OCCSV_FIELDN(6); /* dollar amt */
      WK017=OCCSV_FIELDN(7); /* units */
      TX018=OCCSV_FIELD(8);  /* investment id */
      OCSHOW_LINE('** Processing Plan: ' TX011);
      WK009=PLPLOBJ_GET(PLAN:TX011);
      IF WK009=1;
         IF PMPMOBJ_GET(PLAN:TX011 PRODUCTID:PLPLOBJ_DE(025))>0;
            WK019=PMPMOBJ_NUMDE(401);   /* Helper data (client) */
            IF WK019 = 9;
               TX044 = 'MIDAMER';
            END;
            IF OCFIND(TX012 'De' 'Ba' 'Ro' 'Lp' 'Li' 'Dv') > 0;
               IF ((TX013='D') AND (WK016<0)) OR
                  ((TX013='S') AND (WK017<0));
                  TX052='Y';       /* T301 created */
               ELSE;
                  TX041 = 'Y';     /* T444 created */
               END;
            END;
            IF TX012 = 'Tr';
               TX042 = 'Y';                      /* T444 created */
            END;
            IF OCFIND(TX012 'Dh' 'Di' 'Fo' 'Fe' 'Ld') > 0;
               TX043 = 'Y';                      /* T444 created */
            END;
         END;
      END;
      IF (TX011 <> TX051) AND (TX051 > ' ');
         IF (TX041 = 'Y');
            TX040=OCFMT(SD003 'Z,8')+'.T114';
            VTTHOBJ_VIEW(PLAN:TX051 FILEID:TX040);
            LOOP WHILE VTFHOBJ_NEXT();
                TX055=VTTHOBJ_DE(050);   /* Folder name */
                VTTDOBJ_VIEW(PLAN:TX051 FILEID:TX055);
                LOOP WHILE VTTDOBJ_NEXT();
                    VTTDOBJ_SETDE(DENUM:240 VALUE:TX044);
                    VTTDOBJ_SETDE(DENUM:330 VALUE:TX044);
                    VTTDOBJ_UPDATE();
                ENDLOOP;
            ENDLOOP;
         END;
         IF (TX042 = 'Y');
            TX040=OCFMT(SD003 'Z,8')+'.T381';
            VTTHOBJ_VIEW(PLAN:TX051 FILEID:TX040);
            LOOP WHILE VTFHOBJ_NEXT();
                TX055=VTTHOBJ_DE(050);   /* Folder name */
                VTTDOBJ_VIEW(PLAN:TX051 FILEID:TX055);
                LOOP WHILE VTTDOBJ_NEXT();
                    VTTDOBJ_SETDE(DENUM:240 VALUE:TX044);
                    VTTDOBJ_SETDE(DENUM:330 VALUE:TX044);
                    VTTDOBJ_UPDATE();
                ENDLOOP;
            ENDLOOP;
         END;
         IF (TX043 = 'Y');
            TX040=OCFMT(SD003 'Z,8')+'.T444';
            VTTHOBJ_VIEW(PLAN:TX051 FILEID:TX040);
            LOOP WHILE VTFHOBJ_NEXT();
                TX055=VTTHOBJ_DE(050);   /* Folder name */
                VTTDOBJ_VIEW(PLAN:TX051 FILEID:TX055);
                LOOP WHILE VTTDOBJ_NEXT();
                    VTTDOBJ_SETDE(DENUM:240 VALUE:TX044);
                    VTTDOBJ_SETDE(DENUM:330 VALUE:TX044);
                    VTTDOBJ_UPDATE();
                ENDLOOP;
            ENDLOOP;
         END;
         IF (TX052 = 'Y');
            TX040=OCFMT(SD003 'Z,8')+'.T301';
            VTTHOBJ_VIEW(PLAN:TX051 FILEID:TX040);
            LOOP WHILE VTFHOBJ_NEXT();
                TX055=VTTHOBJ_DE(050);   /* Folder name */
                VTTDOBJ_VIEW(PLAN:TX051 FILEID:TX055);
                LOOP WHILE VTTDOBJ_NEXT();
                    VTTDOBJ_SETDE(DENUM:240 VALUE:TX044);
                    VTTDOBJ_SETDE(DENUM:330 VALUE:TX044);
                    VTTDOBJ_UPDATE();
                ENDLOOP;
            ENDLOOP;
         END;
         TX041='N';TX042='N';TX043='N';TX052='N';
      END;
      TX051 = TX011;                       /* Hold prev. plan */
   ENDLOOP;
   OCSHOW_LINE('** Plan: ' TX051 ' ' TX044);
   OCSHOW_LINE('** t114: ' TX041 ' ' TX044);
   OCSHOW_LINE('** t381: ' TX042 ' ' TX044);
   OCSHOW_LINE('** t444: ' TX043 ' ' TX044);
   OCSHOW_LINE('** t301: ' TX052 ' ' TX044);
   TX040=OCFMT(SD003 'Z,8')+'.T114';
   VTTHOBJ_VIEW(PLAN:TX051 FILEID:TX040);
   LOOP WHILE VTFHOBJ_NEXT();
       TX055=VTTHOBJ_DE(050);   /* Folder name */
       VTTDOBJ_VIEW(PLAN:TX051 FILEID:TX055);
       LOOP WHILE VTTDOBJ_NEXT();
           OCSHOW_LINE('Found 114: ' TX044);
           VTTDOBJ_SETDE(DENUM:240 VALUE:TX044);
           VTTDOBJ_SETDE(DENUM:330 VALUE:TX044);
           VTTDOBJ_UPDATE();
       ENDLOOP;
   ENDLOOP;
   TX040=OCFMT(SD003 'Z,8')+'.T381';
   VTTHOBJ_VIEW(PLAN:TX051 FILEID:TX040);
   LOOP WHILE VTFHOBJ_NEXT();
       TX055=VTTHOBJ_DE(050);   /* Folder name */
       VTTDOBJ_VIEW(PLAN:TX051 FILEID:TX055);
       LOOP WHILE VTTDOBJ_NEXT();
           OCSHOW_LINE('Found 381: ' TX044);
           VTTDOBJ_SETDE(DENUM:240 VALUE:TX044);
           VTTDOBJ_SETDE(DENUM:330 VALUE:TX044);
           VTTDOBJ_UPDATE();
       ENDLOOP;
   ENDLOOP;
   TX040=OCFMT(SD003 'Z,8')+'.T444';
   VTTHOBJ_VIEW(PLAN:TX051 FILEID:TX040);
   LOOP WHILE VTFHOBJ_NEXT();
       TX055=VTTHOBJ_DE(050);   /* Folder name */
       VTTDOBJ_VIEW(PLAN:TX051 FILEID:TX055);
       LOOP WHILE VTTDOBJ_NEXT();
           OCSHOW_LINE('Found 444: ' TX044);
           VTTDOBJ_SETDE(DENUM:240 VALUE:TX044);
           VTTDOBJ_SETDE(DENUM:330 VALUE:TX044);
           VTTDOBJ_UPDATE();
       ENDLOOP;
   ENDLOOP;
   TX040=OCFMT(SD003 'Z,8')+'.T301';
   VTTHOBJ_VIEW(PLAN:TX051 FILEID:TX040);
   LOOP WHILE VTFHOBJ_NEXT();
       TX055=VTTHOBJ_DE(050);   /* Folder name */
       VTTDOBJ_VIEW(PLAN:TX051 FILEID:TX055);
       LOOP WHILE VTTDOBJ_NEXT();
           OCSHOW_LINE('Found 301: ' TX044);
           VTTDOBJ_SETDE(DENUM:240 VALUE:TX044);
           VTTDOBJ_SETDE(DENUM:330 VALUE:TX044);
           VTTDOBJ_UPDATE();
       ENDLOOP;
   ENDLOOP;
ELSE;
    RPLOG_ERROR(MSG:('Error opening file : '+TX045));
END;
