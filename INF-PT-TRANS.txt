************************************************************************************
**
**    TEXT FILE NAME: INF-PT-TRANS
**
**    Purpose: Transaction automation
**
**    Input: Transaction input file
**
**    Ouput: OMNI transactions (T114, T444)
**
**    TEXT/WORK FIELDS
**    _____________________________________
**    TX011  =  Plan (Trade file)
**    TX012  =  Buy/Sell Indicator or trans. code (Trade file)
**    TX013  =  Trade Date (Trade file)
**    TX016  =  Share Quantity (Dollar Amount will be zero)
**    TX017  =  Submission Date
**    WK014  =  Dollar Amount (Trade file)
**    TX020  =  Source
**    TX021  =  Fund
**    TX024  =  SSN
**    TX025  =  Cusip
**    TX026  =  FC095 value
**    TX027  =  PRICE ID
**    TX028  =  Investment ID
**    TX038  =  Firm (Trust) Number
**    TX040  =  Folder name
**    WK010  =  CONTRACT ID
**    WK021  =  Maximun allowed for Omni Contribution or Distribution
**    WK038  =  Grand total Contributions for reporting
**    WK039  =  Grand total Withdrawals for reporting
**    WK060  =  Confirm/reject trailer flag
**    WK061  =  Reject counter
**    WK070  =  Number of confirm/rejects records
**    TX070  =  Input file name
**    TX071  =  Output file name (confirms/rejects)
**    TX072  =  Output file name (report)
**
**
**    CREATED BY                           DATE         WMS
**    ==============================     ===========   ========
**    Glen McPherson                      01/01/2011    4788
**
**    MODIFICATION HISTORY:
**
**    PROGRAMMER                           DATE         WMS
**    ==============================     ========     ========
**    Glen McPherson                      07/01/11     5456
**    Louis Blanchette                    10/18/11     5456
**                      Added Shares X Price to get the Dollars
**                      Always write confirm header and trailer.
**    Louis Blanchette                     09/27/12    8139
**                      Fields update to the T114 and T444 transactions.
**    Rick Sica                            11/06/12    8470
**                      Holzer Conversion to Premier Trust
**    Danny Hagans                         10/22/14    11022 
**                      Shwab  Conversion to Premier Trust
************************************************************************
SD080=999999999;
WK021=9999999.99; /* Omni max for T114 and T444 */
TX070=OCTEXT_GETENV('XJOB')+'/DTCCTRADES';
* always create confirm-reject file
TX071=OCTEXT_GETENV('XJOB')+'/PTCONFIRMREJECT';
SD061=OCFILE2_OPEN(NAME:TX071 MODE:'OUTPUT' TRAILSPACES:'Y');
TX017=OCDATE_FORMAT(SD003 "MMDDYYYY");
TX052='HDR.S01126.E00.C4746.S4746'+TX017+'FUND/SERV -MFS FUND INPUTN001';
OCFILE2_WRITE(TX052);  
WK053=0;WK060=0;WK061=0;WK070=0;WK072=0;
OCSHOW_LINE('Input file=' TX070);
OCDATA_CODESTROY(NAME:'PT_TRUST');
OCDATA_COCREATE(NAME:'PT_TRUST' KEYLENG:4 NUMS:2); /* Firm number,Total Contributions and Total Withdrawals */
IF OCFILE1_OPEN(NAME:TX070 MODE:'INPUT')=1;    /* Trade File */
   OCSHOW_LINE('INSIDE OPEN');
   OCDATA_CODESTROY(NAME:'PTFOLDERS');
   OCDATA_COCREATE(NAME:'PTFOLDERS' KEYLENG:14 TX10S:1);
   OCDATA_CODESTROY(NAME:'PLANFLDR');
   OCDATA_COCREATE(NAME:'PLANFLDR' KEYLENG:6 NUMS:1);
   OCDATA_CODESTROY(NAME:'PLANS450');
   OCDATA_COCREATE(NAME:'PLANS450' KEYLENG:6 TX10S:1);
   OCDATA_CODESTROY(NAME:'PT_CUSIP');
   OCDATA_COCREATE(NAME:'PT_CUSIP' KEYLENG:9 TX10S:1);
   OCDATA_CODESTROY(NAME:'PT_ALT_PLAN_MAPPING');
   OCDATA_COCREATE(NAME:'PT_ALT_PLAN_MAPPING' KEYLENG:15 TX10S:1);
   OCDATA_CODESTROY(NAME:'PT_TRUST');
   OCDATA_COCREATE(NAME:'PT_TRUST' KEYLENG:4 NUMS:2); /* Firm number,Total Contributions and Total Withdrawals */
   TX001=' ';
   TXTXOBJ_VIEW(PLAN:'000001' FILENAME:'PTTRUST' PREFIX:'CT');     /* wms11022 LOAD CONTAINER FROM TEXT FILE DATA */
   LOOP WHILE TXTXOBJ_NEXT();
        TX001=TXTXOBJ_DATA();
        OCDATA_DESET(NAME:'PT_TRUST' DENUM:500 VALUE:OCSUB(TX001 6 2)); /* FIRM*/
        OCDATA_SETDE(NAME:'PT_TRUST' DENUM:1 VALUE:0);  /* Will hold sum of Contributions - Buy */
        OCDATA_SETDE(NAME:'PT_TRUST' DENUM:2 VALUE:0);  /* Will hold sum of Withdrawals - Sell */
        OCDATA_ITEMADD(NAME:'PT_TRUST' KEY:OCSUB(TX001 1 4));          /* FIRM NUMBER */
   ENDLOOP;
   PERFORM 'LOAD.CUSIP';
   TX011='';TX012='';TX013='';TX014='';TX015='';
   WK013=0;WK014=0;WK015=0;
   TX031='N';TX032='N';TX033='N';TX034='N';TX035='N';
   LOOP WHILE OCFILE1_READ(INTO:TX051);
       OCSHOW_LINE('LINE= ' TX051);
       IF (OCSUB(TX051 1 4)='0101');
          IF ((OCSUB(TX051 15 3)<>'999') AND ((OCSUB(TX051 82 3)<>'   ') OR (OCSUB(TX051 5 1)<>'B'))) OR
              (OCSUB(TX051 15 3)='999');
             WK020=1;
          ELSE;
             WK020=0;
          END;
       END;
       IF WK020=0;
          TX015=OCSUB(TX051 3 2);                  /* record type */
          IF (OCSUB(TX051 1 4)='0101');
             TX012=OCSUB(TX051 58 2);              /* transaction code (buy/sell) */
             IF (TX012<>'01') AND (TX012<>'02') AND
                (TX012<>'03') AND (TX012<>'04');
                WK061=+1;                          /* increment reject counter */
                TX034='Y';                         /* invalid transaction code */
                OCSHOW_LINE('INVALID TRANSACTION CODE= ' TX012 ' ' OCSUB(TX051 1 10));
             END;
             TX025=OCSUB(TX051 29 9);  /* Cusip */
             IF OCDATA_ITEMGET(NAME:'PT_CUSIP' KEY:TX025)=0; /* Validate Cusip exist in Omni */
                WK061=+1;                          /* increment reject counter */
                TX035='Y';                         /* invalid Cusip */
                OCSHOW_LINE('CUSIP NOT FOUND IN PT.CUSIP ' TX025);
             END; 
             WK015=1;
             OCCLIP_CLEAR();
             OCCLIP_ADDLINE(TX051);
             TX038=OCSUB(TX051 8 4);                           /* Firm Number */
             TX013=OCSUB(TX051 68 8);              /* trade date */
             TX043='';
             IF OCTEXT_TRIM(TX013)<>'';
                TX043=OCSUB(TX013 5 4)+OCSUB(TX013 1 4);
                IF OCNUM_VALUE(TX043)>0;
                   WK013=OCTEXT_TONUM(TX043);      /* trade date - numeric */
                   IF OCDATE_VALID(WK013);         /* valid trade date check */
                     TX032='N';
                   ELSE;
                     WK061=+1;                     /* increment reject counter */
                     TX032='Y';
                     OCSHOW_LINE('INVALID TRADE DATE= ' TX043);
                   END;
                ELSE;
                   WK061=+1;                       /* increment reject counter */
                   TX032='Y';
                   OCSHOW_LINE('INVALID TRADE DATE= ' TX043);
                END;
             ELSE;
                WK061=+1;                          /* increment reject counter */
                TX032='Y';
                OCSHOW_LINE('INVALID TRADE DATE= ' TX043);
             END;
          END;
          IF TX015='02';
             OCCLIP_ADDLINE(TX051);
             TX011=OCSUB(TX051 43 6);                   /* customer acct number */
             IF OCDATA_ITEMGET(NAME:'PT_ALT_PLAN_MAPPING' KEY:(TX011+TX025))>0;
                TX011=OCDATA_DE(NAME:'PT_ALT_PLAN_MAPPING' DENUM:500);
             END;
             OCSHOW_LINE('** Processing Plan: ' TX011);
             IF (PLPLOBJ_GET(PLAN:TX011)=0);            /* valid plan check */
                WK061=+1;                               /* increment reject counter */
                TX031='Y';
                OCSHOW_LINE('PLAN NOT FOUND= ' TX011);
             ELSE;
                IF AAAAOBJ_GET(PLAN:TX011 PREFIX:'  ' ADDRESSID:('REM' + TX011) SEQNUM:001)>0;
                   TX045=OCSUB(AAAAOBJ_DE(DENUM:450) 1 4);
                ELSE;
                   TX045='UNKNOWN';
                END;
*                IF (TX045<>'5982') AND (TX045<>'0068') AND (TX045<>'5954');
                IF OCDATA_ITEMGET(NAME:'PT_TRUST' KEY:TX045)=0;   /* Not a PREMIER TRUST Firm */
                   IF OCDATA_ITEMGET(NAME:'PLANS450' KEY:TX045)=0;
                      OCDATA_SETDE(NAME:'PLANS450' DENUM:500 VALUE:TX045);
                      OCDATA_SETDE(NAME:'PLANS450' DENUM:1 VALUE:0);  /* Will hold sum of Contributions - Buy */
                      OCDATA_SETDE(NAME:'PLANS450' DENUM:2 VALUE:0);  /* Will hold sum of Withdrawals - Sell */
                      OCDATA_ITEMADD(NAME:'PLANS450' KEY:TX045);
                   END;
                END;
                TX060='ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                TX044=TX011+TX043;
                IF OCDATA_ITEMGET(NAME:'PLANFLDR' KEY:TX011)=0;
                   OCDATA_SETDE(NAME:'PLANFLDR' DENUM:1 VALUE:1);
                   OCDATA_ITEMADD(NAME:'PLANFLDR' KEY:TX011);
                   TX061='A';
                   OCDATA_SETDE(NAME:'PTFOLDERS' DENUM:500 VALUE:'A');
                   OCDATA_ITEMADD(NAME:'PTFOLDERS' KEY:TX044);
                ELSE;   
                   WK053=OCDATA_NUMDE(NAME:'PLANFLDR' DENUM:1);
                   IF OCDATA_ITEMGET(NAME:'PTFOLDERS' KEY:TX044)=0;
                      WK053=+1;
                      TX061=OCSUB(TX060 WK053 1);
                      OCDATA_SETDE(NAME:'PTFOLDERS' DENUM:500 VALUE:TX061);
                      OCDATA_ITEMADD(NAME:'PTFOLDERS' KEY:TX044);
                      OCDATA_SETDE(NAME:'PLANFLDR' DENUM:1 VALUE:WK053);
                      OCDATA_ITEMUPDATE(NAME:'PLANFLDR' KEY:TX011);
                   END;    
                END;
                TX016=OCSUB(TX051 8 14);    /* Share Quantity */
                IF OCNUM_VALUE(TX016)>0;
                   TX016=OCSUB(TX051 8 10)+'.'+ OCSUB(TX051 18 4);
                   WK018=OCTEXT_TONUM(TX016);  /* Transfer to numeric */
                ELSE;
                   WK018=0;
                END;   
             END;
          END;
          IF TX015='03';
             OCCLIP_ADDLINE(TX051);
             TX014=OCSUB(TX051 8 16);                   /* dollar amount */
             IF (OCNUM_VALUE(TX014)>0) OR (WK018>0);
                WK014=OCTEXT_TONUM(TX014);  /* dollar amount - numeric */
                WK016=0;WK017=0;
                IF WK014>0;
                   WK014=WK014/100;
                   PERFORM 'GET.PRICE';
                ELSE;
                   PERFORM 'GET.PRICE';
                   WK019=WK018*WK016;   /* Dollar Amount = Shares x price */
                   WK014=OCNUM_ROUND(WK019 2);
                END;
                IF (WK014>WK021) OR ((WK018>0) AND (WK016=0));
                   WK061=+1;                /* increment reject counter */
                   TX033='Y';
                   IF WK014>WK021;
                      OCSHOW_LINE('DOLLAR AMOUNT= ' WK014 ' EXCEEDS LIMIT ' WK021);
                   ELSE;
                      OCSHOW_LINE('DOLLAR AMOUNT NOT CALCULATED; UNITS=' WK018);
                   END;
                ELSE;   
                   TX033='N';
                END;   
             ELSE;
                WK061=+1;                               /* increment reject counter */
                TX033='Y';
                OCSHOW_LINE('INVALID AMOUNT= ' TX014 ' SHARE ' WK018);
             END;
          END;
          IF (TX015='04') OR (TX015='05') OR (TX015='06');
             OCCLIP_ADDLINE(TX051);
          END;
          IF (OCSUB(TX051 1 1)='9') AND (WK015>0);
             PERFORM 'PROCESS.TRADE.REC';
          END;   
       END;
   ENDLOOP;
   TX052='END.S01126.E00.C4746.S4746'+OCFMT(WK070 'Z,7');
   OCFILE2_WRITE(TX052);      /* write confirm-reject trailer */
   OCFILE1_CLOSE();    /* Trade File */
* Write sub-totals to the PTTRADES report file   
   IF WK072=1;         /* Something was written to the file */
      TX073=' ';
      OCFILE3_WRITE(TX073);  
      WK038=0;WK039=0;    /* Initialize Grand Totals */
      OCDATA_ITEMVIEW(NAME:'PT_TRUST');
      LOOP WHILE OCDATA_ITEMNEXT(NAME:'PT_TRUST');
         WK014=OCDATA_NUMDE(NAME:'PT_TRUST' DENUM:1);
         WK038=+WK014;  /* Add up the Contributions for the Grand Total Contributions */
         TX073=OCTEXT_SPACES(27)+ 'Trust: '+ OCDATA_ITEMKEY(NAME:'PT_TRUST')+ ' ' +'Total Contributions : '+ OCFMT_CASH(WK014);
         OCFILE3_WRITE(TX073);  
         WK014=OCDATA_NUMDE(NAME:'PT_TRUST' DENUM:2);
         WK039=+WK014;  /* Add up the Withdrawals for the Grand Total Withdrawals */
         TX073=OCTEXT_SPACES(47)+ 'Withdrawals : '  + OCFMT_CASH(WK014);
         OCFILE3_WRITE(TX073);
         TX073=' ';
         OCFILE3_WRITE(TX073);  
      ENDLOOP;
      TX073=' ';
      OCFILE3_WRITE(TX073);  
      TX073=OCTEXT_SPACES(33)  + 'Grand Total Contributions : ' + OCFMT_CASH(WK038);
      OCFILE3_WRITE(TX073);  
      TX073=OCTEXT_SPACES(47)  + 'Withdrawals : ' + OCFMT_CASH(WK039);
      OCFILE3_WRITE(TX073);  
   END;
   OCFILE3_CLOSE();    /* PTTRADES.txt Report File */
** Any AA450 issues
   WK011=0;
   OCDATA_ITEMVIEW(NAME:'PLANS450');
   LOOP WHILE OCDATA_ITEMNEXT(NAME:'PLANS450');
       TX011=OCDATA_ITEMKEY(NAME:'PLANS450');
       IF TX011>'';
          IF WK011=0;
             WK011=1;
             TX002 = "$XJOB/AA450.TXT";         /* AA450 issues file */
             IF OCFILE4_OPEN(Name:TX002 Mode:"OUTPUT") = 0;
                OCSHOW_LINE(" !! Error opening " TX002);
             END;
          END;
          TX003='TRUST COMPANY VALUE (AA450) MISSING OR INVALID FOR PLAN: '+TX011+ '  ';
          TX003=TX003+'VALUE: '+OCDATA_DE(NAME:'PLANS450' DENUM:500);
          OCFILE4_WRITE(TX003);
       END;
    ENDLOOP;
    IF WK011=1;
       OCFILE4_CLOSE();
    END;
    OCDATA_CODESTROY(NAME:'PTFOLDERS');
    OCDATA_CODESTROY(NAME:'PLANFLDR');
    OCDATA_CODESTROY(NAME:'PLANS450');
    OCDATA_CODESTROY(NAME:'PT_CUSIP');
    OCDATA_CODESTROY(NAME:'PT_ALT_PLAN_MAPPING');
    OCDATA_CODESTROY(NAME:'PT_TRUST');
ELSE;
    OCSHOW_LINE('ERROR OPENING FILE');
    RPLOG_ERROR(MSG:('Error opening trade file: '+TX070));
END;
* always create2 confirm-reject file close
OCFILE2_CLOSE(); 
**
*******************************************************************************************************
**
** SUBROUTINES
**
*******************************************************************************************************
** ROUTINE 'PROCESS.TRADE.REC';
*******************************************************************************************************
ROUTINE 'PROCESS.TRADE.REC';
   IF WK061>0;
      PERFORM 'WRITE.CONFIRM.REJECT';
   ELSE;
      WK020=0;
      SOSCOBJ_VIEW(PLAN:TX011);
      LOOP WHILE SOSCOBJ_NEXT();
          TX020 = SOSCOBJ_DE(007);   /* source */
          WK020 = WK020 + 1;         /* should only be one source */
      ENDLOOP;
      IF WK020=1;
         IF (PMPMOBJ_GET(PLAN:TX011 PRODUCTID:PLPLOBJ_DE(025))>0);
            PTPHOBJ_VIEW(PLAN:TX011);
            IF PTPHOBJ_NEXT();
               TX024=PTPHOBJ_DE(007);
            END;
            TX021 = '**' + TX020;           /* fund */
            IF (TX012='01') OR (TX012='02');
               PERFORM 'CREATE.114';       /* create deposit transaction */
            ELSE;
               PERFORM 'CREATE.444';       /* create withdrawal transaction */
            END;
         ELSE;
            WK061=+1;
            OCSHOW_LINE('PRODUCT MASTER NOT FOUND FOR ' TX011);
         END;
      ELSE;
         WK061=+1;
         OCSHOW_LINE('MORE THAN ONE SOURCE ON PLAN ' TX011);
      END;
      PERFORM 'WRITE.CONFIRM.REJECT';
   END;
   TX031='N';TX032='N';TX033='N';TX034='N';TX035='N';
GOBACK;
*******************************************************************************************************
** ROUTINE : CREATE.114
*******************************************************************************************************
ROUTINE 'CREATE.114';
** T114 Contribution
TX061='';
IF OCDATA_ITEMGET(NAME:'PTFOLDERS' KEY:TX044)>0;
   TX061=OCDATA_DE(NAME:'PTFOLDERS' DENUM:500);
END;
TX040=OCFMT(SD003 'Z,8')+'.S114'+TX061;
OCSHOW_LINE('** Creating T114 ' TX040);
VTFHOBJ_INIT(PLAN:TX011 FILEID:TX040);
VTFHOBJ_SETDE(DENUM:370 VALUE:WK013); /* Override Trade Date W8139 */
VTFHOBJ_SETDE(DENUM:590 VALUE:'B');   /* Report Indicator W8139 */
VTFHOBJ_SETDE(DENUM:330 VALUE:'M1');
VTFHOBJ_ADD();
BATIUT_INIT(PLAN:TX011 PARTID:TX024 TRAN:'114' DATE:WK013 FUND:' ');
BATIUT_SETDE(050 TX021);        /* contrib fund id */
BATIUT_SETDE(065 WK014);        /* amount */
BATIUT_SETDE(112 'A');          /* usage code 3 W8139 */
BATIUT_SETVTDE(240 VALUE:'MCAK'); /* Create UserId W8139 */
BATIUT_SETVTDE(330 VALUE:'MCAK'); /* Update UserId W8139 */
BATIUT_ADDTRAN();
BATIUT_TOVTRAN(FILEID:TX040 STEP:'M1');
IF WK072=0;
   TX072=OCTEXT_GETENV('XJOB')+'/PTTRADES.txt';
   IF OCFILE3_OPEN(NAME:TX072 MODE:'OUTPUT')>0;
      WK072=1;
      TX073='PLAN    CONTRIBUTION/WITHDRAWAL        SHARES        PRICE       DOLLAR AMOUNT     FOLDER';
      OCFILE3_WRITE(TX073);  
      TX073=' ';
      OCFILE3_WRITE(TX073);  
   END;   
END;
IF WK017=0;
   WK017=WK018;
END;   
TX073=TX011+'  '+'CONTRIBUTION'+OCTEXT_SPACES(11) + OCFMT(WK017 "F3,9.4") +OCTEXT_SPACES(02)+ OCFMT(WK016 "F5.6") +'  '+OCFMT_CASH(WK014)+'     '+TX040;
OCFILE3_WRITE(TX073); 
* Add Contribution to the Firm Container for Sub-total 
IF OCDATA_ITEMGET(NAME:'PT_TRUST' KEY:TX038)=1;
   OCDATA_DEADD(NAME:'PT_TRUST' DENUM:1 VALUE:WK014);
   OCDATA_ITEMUPDATE(NAME:'PT_TRUST' KEY:TX038);
END;
GOBACK;
*******************************************************************************************************
** ROUTINE : CREATE.444
*******************************************************************************************************
ROUTINE 'CREATE.444';
** T444 Withdrawal
TX040=OCFMT(SD003 'Z,8')+'.S444';
OCSHOW_LINE('** Creating T444 ' TX040);
BATIUT_INIT(PLAN:TX011 PARTID:TX024 TRAN:'444' DATE:WK013 FUND:' ');
BATIUT_SETDE(060 '1');            /* exact total flag */
BATIUT_SETDE(065 WK014);          /* dollar amount */
BATIUT_SETDE(100 '0');            /* dollars wd */
BATIUT_SETDE(221 '1');            /* dollar withdrawal */
BATIUT_SETDE(231 WK014);          /* dollar amount */
BATIUT_SETDE(070 '01');           /* max calc */
BATIUT_SETDE(115 'A');            /* usage code 1 */
BATIUT_SETDE(130 0);              /* check status */
BATIUT_SETDE(140 '0');            /* gross adj calc */
BATIUT_SETDE(168 0);              /* alt addr indicator */
BATIUT_SETDE(181 TX021);          /* fund id */
BATIUT_SETDE(155 '1');            /* Mandatory withholding flag W8139 */
BATIUT_SETDE(055 VALUE:"1" TRLR:"WHLD"); /* Federal Override Flag W8139 */
BATIUT_SETDE(100 VALUE:"1" TRLR:"WHLD"); /* State Override Flag W8139 */
BATIUT_SETVTDE(240 VALUE:'MCAK'); /* Create UserId W8139 */
BATIUT_SETVTDE(330 VALUE:'MCAK'); /* Update UserId W8139 */
BATIUT_SETVTDE(580 VALUE:'K');    /* usage code */
BATIUT_SETVTDE(581 VALUE:'K');    /* usage code 1 */
BATIUT_ADDTRAN();
BATIUT_ADDTRLR();
BATIUT_TOVTRAN(FILEID:TX040 STEP:'M3');
IF WK072=0;
   TX072=OCTEXT_GETENV('XJOB')+'/PTTRADES.txt';
   IF OCFILE3_OPEN(NAME:TX072 MODE:'OUTPUT')>0;
      WK072=1;
      TX073='PLAN    CONTRIBUTION/WITHDRAWAL        SHARES        PRICE       DOLLAR AMOUNT     FOLDER';
      OCFILE3_WRITE(TX073);  
      TX073=' ';
      OCFILE3_WRITE(TX073);  
   END;   
END;
IF WK017=0;
   WK017=WK018;
END;   
TX073=TX011+'  '+'WITHDRAWAL'+OCTEXT_SPACES(13) + OCFMT(WK017 "F3,9.4") +OCTEXT_SPACES(02)+ OCFMT(WK016 "F5.6") +'  '+OCFMT_CASH(WK014)+'     '+TX040;
OCFILE3_WRITE(TX073);  
* Add Withdrawals to the Firm Container for Sub-total 
IF OCDATA_ITEMGET(NAME:'PT_TRUST' KEY:TX038)=1;
   OCDATA_DEADD(NAME:'PT_TRUST' DENUM:2 VALUE:WK014);
   OCDATA_ITEMUPDATE(NAME:'PT_TRUST' KEY:TX038);
END;
GOBACK;
*******************************************************************************************************
** ROUTINE : WRITE.CONFIRM.REJECT
*******************************************************************************************************
ROUTINE 'WRITE.CONFIRM.REJECT';
   WK060=+1;                       /* increment confirm-reject counter */
   IF WK061>0; 
      PERFORM 'WRITE.REJECT';
   ELSE;
      PERFORM 'WRITE.CONFIRM';
   END;
   WK061=0;
GOBACK;
*******************************************************************************************************
** ROUTINE : WRITE.CONFIRM
*******************************************************************************************************
ROUTINE 'WRITE.CONFIRM';
   WK050=0;TX052='';
   OCCLIP_VIEW();
   LOOP WHILE OCCLIP_NEXT();
       TX052 = OCCLIP_LINE();
       IF OCSUB(TX052 3 2)<>'01';
          TX057=OCSUB(TX052 2 1);
          IF TX057='3';
             TX058='4';
          ELSE;
             IF TX057='4';
                TX058='5';
             ELSE;
                IF TX057='5';
                   TX058='6';
                ELSE;
                   TX058='7';
                END;
             END;   
          END;
       END;   
*       IF (OCSUB(TX052 15 3) <> '999');
          IF OCSUB(TX052 3 2)='01';
             TX051='0101F'+OCSUB(TX052 6 11)+'5'+OCSUB(TX052 18 42)+TX017+OCSUB(TX052 68 33);
             TX025=OCSUB(TX051 29 9);  /* Cusip */
             WK050=+1;
          ELSE;
             IF OCSUB(TX052 3 2)='02';
                TX053=TX052;
                TX016=OCSUB(TX052 8 14);    /* Share Quantity */
                IF OCNUM_VALUE(TX016)>0;
                   TX016=OCSUB(TX052 8 10)+'.'+ OCSUB(TX052 18 4);
                   WK018=OCTEXT_TONUM(TX016);  /* Transfer to numeric */
                ELSE;
                   WK018=0;
                END;   
             ELSE;  
                IF OCSUB(TX052 1 1)='9';
                   TX052='0'+OCSUB(TX052 2 99);
                END;   
                TX051=TX052;
             END;   
          END;
          IF OCSUB(TX052 3 2)<>'02';
             IF OCSUB(TX052 3 2)='03';
                IF WK018>0;
                   TX029=OCTEXT_TRIM(OCSUB(OCFMT(WK018 'ZONED' ROUND:1) 3 14));
                END;   
                TX054=OCSUB(TX053 1 7)+TX029+OCSUB(TX053 22 79);
                TX056=OCSUB(TX052 8 16);
                IF (OCNUM_VALUE(TX056)=0) AND (WK018>0); /* Convert Shares to Dollar */
                   WK019=WK018*WK016;   /* Dollar Amount = Shares x price */
                   WK014=OCNUM_ROUND(WK019 2);
                   TX056=OCFMT(WK014 "Z,12V2");
                   TX051=OCSUB(TX051 1 9)+TX056+OCSUB(TX051 24 79); 
                END;
                OCFILE2_WRITE(TX054);     /* write confirm detail */  
                WK070=+1;   
             END; 
             IF (OCSUB(TX052 3 2)='01') AND (WK050>1);
                 TX054='9'+TX058+'06   0000000000000000          ';
                 IF OCTEXT_LENGTH(TX056)=14;
                    TX056='00'+TX056;
                 END;   
                 TX055=TX054+TX056;
                 TX054=TX055+TX023;
                 TX054=TX054+'                                      0';
                 OCFILE2_WRITE(TX054);     /* write record 6 */  
                 WK070=+1;   
             END;    
             OCFILE2_WRITE(TX051);     /* write confirm detail */  
             WK070=+1;   
          END;   
*       END;
   ENDLOOP;
   IF WK070>0;
      TX054='9'+TX058+'06   0000000000000000          ';
      IF OCTEXT_LENGTH(TX056)=14;
         TX056='00'+TX056;
      END;   
      WK097=OCTEXT_LENGTH(TX056);
      TX055=TX054+TX056;
      TX054=TX055+TX023;
      TX054=TX054+'                                      0';
      OCFILE2_WRITE(TX054);     /* write record 6 */  
      WK070=+1;   
   END;    
GOBACK;
*******************************************************************************************************
** ROUTINE : WRITE.REJECT
*******************************************************************************************************
ROUTINE 'WRITE.REJECT';
   OCCLIP_VIEW();
   LOOP WHILE OCCLIP_NEXT();
       TX052 = OCCLIP_LINE();
       IF (OCSUB(TX052 3 2)='01') AND (OCSUB(TX052 10 3) <> '999') AND
          (OCSUB(TX052 10 3) <> '000');
          TX055='';
          IF TX031='Y';
             TX055='011';           /* set reject code - plan number */
          ELSE;
             IF TX032='Y';
                TX055='016';        /* set reject code - trade date */
             ELSE;
                IF TX033='Y';
                   TX055='019';     /* set reject code - dollar or share amt */
                ELSE;
                   IF TX034='Y';
                      TX055='018';  /* set reject code - transaction code */
                   ELSE;
                      IF TX035='Y';
                         TX055='007';  /* set reject code - Security Code Cusip */
                      END;   
                   END;
                END;
             END;
          END;
          IF WK061=1;
             TX051='9101F'            +
                   OCSUB(TX052 6 9)   +
                   '100'              +     /* fund reject */
                   OCSUB(TX052 18 39) +
                   '   '              +
                   OCSUB(TX052 60 8)  +
                   '    '             +
                   '1'                +     /* single error */
                   TX055              +     /* error code */
                   '     '            +
                   OCSUB(TX052 81 12);
             OCFILE2_WRITE(TX051);          /* write reject detail */ 
             WK070=+1;    
          ELSE;
             TX051='0101F'  +
                   OCSUB(TX052 6 9)   +
                   '100'              +     /* fund reject */
                   OCSUB(TX052 18 39) +
                   '   '              +
                   OCSUB(TX052 60 8)  +
                   '    '             +
                   '2'                +     /* multiple errors */
                   TX055              +     /* error code */
                   '     '            +
                   OCSUB(TX052 81 12);
             OCFILE2_WRITE(TX051);          /* write reject detail */  
             WK070=+1;   
             TX056='';
             IF (TX035='Y') AND (TX055<>'007');
                TX056=TX056+'007';
             END;
             IF (TX031='Y') AND (TX055<>'011');
                TX056=TX056+'011';
             END;
             IF (TX032='Y') AND (TX055<>'016');
                TX056=TX056+'016';
             END;
             IF (TX034='Y') AND (TX055<>'018');
                TX056=TX056+'018';
             END;
             IF (TX033='Y') AND (TX055<>'019');
                TX056=TX056+'019';
             END;
             TX051='9202F  '+TX056;
             OCFILE2_WRITE(TX051);          /* write reject detail line 2 */  
             WK070=+1;   
          END;
          WK061=0;
       END;
   ENDLOOP;    
GOBACK;
*******************************************************************************************************
** ROUTINE : GET.PRICE
*******************************************************************************************************
ROUTINE 'GET.PRICE';
IF (TX031='N') AND (TX035='N');
   OCDATA_ITEMGET(NAME:'PT_CUSIP' KEY:TX025); /* Get the Contract Account ID associated with the Cusip */
   TX026=OCDATA_DE(NAME:'PT_CUSIP' DENUM:500);
   TX027='';TX028='';TX029='';
   FNFCOBJ_VIEW(PLAN:TX011);
   LOOP WHILE FNFCOBJ_NEXT() AND (TX027='');
      IF TX026=FNFCOBJ_DE(095);
         TX028=OCSUB(FNFCOBJ_DE(026) 1 2);
         IF IVICOBJ_GET(PLAN:TX011 FUNDIV:TX028);
            TX027=IVICOBJ_DE(220);
         END;
      END;
   ENDLOOP;
   IF TX027='';
      OCSHOW_LINE('PRICE ID NOT FOUND ON PLAN ' TX011 ' FOR ' TX026);
   ELSE;
      WK010=0;
      SACIOBJ_VIEW(PLAN:'000001'); /* WMS1519B */
      LOOP WHILE SACIOBJ_NEXT() AND (WK010=0);
         IF (SACIOBJ_DE(008)=TX028) AND
            (SACIOBJ_DE(100)=TX026) AND
            (WK013>=SACIOBJ_NUMDE(140)) AND
            (WK013<=SACIOBJ_NUMDE(150));
            WK010=SACIOBJ_NUMDE(025);
         END;
      ENDLOOP;
      IF WK010>0;
         IF PRDROBJ_GET(PLAN:'000001' CONTRACT:WK010 PRICEID:TX027 TRADEDATE:WK013);
            WK016=PRDROBJ_NUMDE(DENUM:100);  /* price */
            TX023=OCFMT(WK016 "Z,6V6");
            IF WK014>0;
               WK017=OCNUM_ROUND((WK014/WK016) 4);
            ELSE;
               WK017=WK018;
            END;
            TX029=OCTEXT_TRIM(OCSUB(OCFMT(WK017 'ZONED' ROUND:1) 3 14));
         END;
      ELSE;
         OCSHOW_LINE('CONTRACT ID NOT FOUND ON PLAN ' TX011 ' FOR ' TX026);
      END;
   END;
END;
GOBACK;
*******************************************************************************************************
** ROUTINE : LOAD.CUSIP
*******************************************************************************************************
ROUTINE 'LOAD.CUSIP';
TXTXOBJ_VIEW(PLAN:'000001' FILENAME:'PTCUSIP' PREFIX:'CT');
LOOP WHILE TXTXOBJ_NEXT();
  TX006=TXTXOBJ_DATA(); 
  IF TX006 <> '';
     TX036=OCSUB(TX006 1 3);  /*  Contract Account ID Fixed */
     TX037=OCSUB(TX006 5 9);  /* Cusip */ 
     OCDATA_DESET(NAME:'PT_CUSIP' DENUM:500 VALUE:TX036);
     OCDATA_ITEMADD(NAME:'PT_CUSIP' KEY:TX037);
  END;
ENDLOOP;
TXTXOBJ_VIEW(PLAN:'000001' FILENAME:'PTALTPLANMAP' PREFIX:'CT');
LOOP WHILE TXTXOBJ_NEXT();
  TX006=TXTXOBJ_DATA(); 
  IF OCSUB(TX006 1 1)='I';     /* Incoming plans */
     TX036=OCSUB(TX006 2 15);  /* Key (plan used by trust + cusip) */
     TX037=OCSUB(TX006 18 6);  /* Alternate Plan */ 
     OCDATA_DESET(NAME:'PT_ALT_PLAN_MAPPING' DENUM:500 VALUE:TX037);
     OCDATA_ITEMADD(NAME:'PT_ALT_PLAN_MAPPING' KEY:TX036);
  END;
ENDLOOP;
GOBACK;