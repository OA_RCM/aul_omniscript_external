************************************************************************************************
**
**    Text File Name: ONE-RCF-POSTCONV.txt
**
**    This calculator will convert AUL's RETIREMENT file to SG OMNI 5.8s CT.RETIREMENT file
**
**    Variables:
**
**    WK001=AUL RETIREMENT FIELD 2 (AGE)          TX001=PLAN
**    WK002=AUL RETIREMENT FIELD 3 (MIN SVC)      TX002=AUL RETIREMENT FILE RECORD
**    WK003=ERROR FLAG                            TX003=AUL RETIREMENT FIELD 1 (TEXT DESC)
**    WK004=EARLY PROJ NORMAL RETIREMENT DATE     TX004=DISABILITY DESC
**    WK005=EARLY PROJ EARLY RETIREMENT DATE      TX005=ALT DISABILITY DESC
**    WK006=ALT EARLY PROJ NORMAL RETIREMENT DATE TX006=ALT NORMAL
**    WK007=ALT EARLY PROJ EARLY RETIREMENT DATE  TX007=ALT EARLY
**    WK008=LOOPER VARIABLE                       TX008=CT.RETIREMENT SVC RECORD STRING
**    WK009=CLIP LINE NUMBER VARIABLE             TX009=CT.RETIREMENT AGE RECORD STRING
**    WK010=                                      TX010=PM550
**    WK011=                                      TX011=02 VESTING SCENARIO CONSTANT SEARCH VALUE
**    WK012=                                      TX012=PARTICIPANT ID
**    WK013=                                      TX013= FIRST 8 DIGITS OF PARTID
**    WK014=                                      TX014=
**    WK015=                                      TX015=
**    WK016=                                      TX016=
**    WK017=                                      TX017=
**    WK018=                                      TX018=
**    WK019=                                      TX019=
**    WK020=                                      TX020= 
**    WK021=                                      TX021= ENVIRONMENT PREFIX
**    WK022=                                      TX022= (R)EPORT OR (U)PDATE FLAG
**    WK070 =                                     TX023= REPORT or UPDATE TEXT HEADER
**    WK083 = DISABILITY AGE 
**    WK084 = DISABILITY SVC
**    WK085 = ALT DISABILITY AGE
**    WK086 = ALT NORMAL AGE  
**    WK087 = ALT NORMAL SVC
**    WK088 = ALT DISABILITY SVC
**    WK089 = ALT EARLY  AGE
**    WK095 = ALT EARLY  SVC
**    WK096 = 
**    WK099= REPORT LINE COUNTER                  
**
**    
**    Created by                           Date         SMR
**    ==============================     ========     ========
**    Judy Stewart                       09/10/2009   WMS1519B
**
**    Modification History:
**
**    Programmer                           Date         SMR
**    ==============================     ========     ========
***********************************************************************************************
**
** INITIALIZATIONS
**
SD080=9999999;
WK003=0;WK022=0;WK023=0;WK099=0;
TX011='02';
TX022='U'; /* R = report , U = update */
*TX022='R'; /* R = report , U = update */
TX021=OCTEXT_GETENV('EBSRPTPFX');                 /* ENVIRONMENT PREFIX */
**
** INPUTS
**
TX001=OCTEXT_GETENV('OMNIrcfPLAN');               /* SO 1 PLAN CAN BE RUN */
OCSHOW_LINE('$OMNIrcfPLAN => ' TX001);
OCPAGE_SETUP(RPTSEQ:991 PLAN:SD001);              /* AUL RCF REPORT */
**
** MAIN
**
IF (TX001='ALL');
  PLPLOBJ_VIEW(PLANLO:'A');
ELSEIF (TX001<>'');
  IF PLPLOBJ_GET(PLAN:TX001)>0;
    PLPLOBJ_VIEW(PLAN:TX001);
  ELSE;
    WK003=1; /* ERROR */
  END;
ELSE;
  WK003=1;   /* ERROR */
END;
IF WK003<>1; /* NO ERROR */
  LOOP WHILE PLPLOBJ_NEXT(); 
    TX001=PLPLOBJ_DE(011);
    WK010=0;WK011=0; /* FILE FOUND ERROR FLAGS */
    TX014='';        /* RESET REPORT MESSAGE */
    OCSHOW_LINE('Examining Plan#:  ' TX001 ' ...'); 
    IF PMPMOBJ_GET(PLAN:TX001 PRODUCTID:PLPLOBJ_DE(025))>0;
      IF OCTEXT_FIND(TX011 PMPMOBJ_DE(550))>0; /* VESTING SCENARIO 02 PLANS */
        OCSHOW_LINE('>>PLAN HAS VESTING SCENARIO 02');
        TX010=PMPMOBJ_DE(550);
        PERFORM 'LOAD.AULRETIREMENT'; 
        PERFORM 'LOAD.CTRETIREMENT';
        IF (WK010=1) AND (WK011=1);       /* PLAN MUST HAVE BOTH RETIREMENT FILES */
          WK022=+1;                       /* PLAN COUNTER */
          PERFORM 'DISPLAY.CLIP';         /* P6JLS UNIT TESTING */
          PERFORM 'ADD.TOCLIP';
          PERFORM 'DISPLAY.CLIP';         /* P6JLS UNIT TESTING */
          PERFORM 'DELETE.AULRETIREMENT'; 
          PERFORM 'CLIPTO.CTRETIREMENT';
          PERFORM 'PM550.UPDATE';
          PERFORM 'PL886.UPDATE';
          PERFORM 'PART.LOOP';
          TX014='AUL RETIREMENT FILE SUCCESSFULLY MERGED WITH CT.RETIREMENT FILE.';
          PERFORM 'REPORT.DETAIL';
        ELSE;
          WK023=+1;                       /* ERROR COUNTER */
          IF WK010=0;
            TX014 = 'AUL RETIREMENT FILE NOT FOUND FOR PLAN: ';
          ELSEIF WK011=0;
            TX014 = 'CT.RETIREMENT FILE NOT FOUND FOR PLAN: ';
          END;
          OCSHOW_LINE(TX014 TX001 ' -- SKIPPED!!');
          PERFORM 'REPORT.DETAIL';
        END;
      END; /* PM550 CONTAINS VESTING SCENARIO 2 */
    ELSE;
      TX014 = 'PRODUCT MASTER RECORD NOT FOUND FOR THE PLAN';
      PERFORM 'REPORT.DETAIL';
      OCSHOW_LINE('PM RECORD DOES NOT EXIST FOR PLAN : ' TX001);
    END;   /* IF PM RECORD EXISTS FOR PLAN */
  ENDLOOP; /* PLAN LOOP */
ELSE;
  OCSHOW_LINE('$OMNIrcfPLAN must be <ALL> or a valid plan number <G12345>. Export via UNIX and rerun!!!');
  TX014 = '$OMNIrcfPLAN environment variable not defined properly.  Must be ALL or valid plan#.';
  PERFORM 'REPORT.DETAIL';
  QUIT;
END;
**
** END OF MAIN
**
**
** WRITE COUNT OF TOTAL TRANSACTIONS
**
  PERFORM 'REPORT.TOTALS';
**
** OUTPUT PAGE IN BUFFER
**
  IF WK099 > 0;
    OCPAGE_WRITE(RPTSEQ:991);
  END;
**
**
**
******************************************
ROUTINE 'LOAD.AULRETIREMENT';
******************************************
**OCSHOW_LINE('LOAD.AULRETIREMENT');
OCSHOW_LINE('=========================AUL RETIREMENT FILE========================');
TXTXOBJ_VIEW(PLAN:TX001 PREFIX:'  ' FILENAME:'RETIREMENT');  /* AULs RETIREMENT FILE */
LOOP WHILE TXTXOBJ_NEXT();
  WK010=1; /* PLAN HAS AUL RETIREMENT FILE */
  TX003='99999';WK001=99999;WK002=99999;
  TX002 = TXTXOBJ_DATA();
  IF (TX002>'');  /* RETIREMENT RECORD CANNOT BE BLANK */
    OCSHOW_LINE(TX002);
    OCCSV_RESET();
    OCCSV_SETLINE(TX002);
    TX003 = OCCSV_FIELD(1);   /* TEXT DESC */
    OCTEXT_REPLACEALL(TX003 '-' ' '); /* REMOVE HYPHEN REPLACE WITH SPACE */
    WK001 = OCCSV_FIELDN(2);  /* AGE */
    WK002 = OCCSV_FIELDN(3);  /* MIN SVC */
    IF (TX003>'') AND ((TX003='DISABILITY') OR (TX003='ALT DISABILITY') OR (TX003='ALT NORMAL') OR 
       (TX003='ALT EARLY'));  /* TEXT DESCRIPTION CANNOT BE BLANK */
      OCSHOW_LINE('TX003 = ' TX003);
      IF TX003 = 'DISABILITY';  
         TX004 = TX003;         /* DISABILITY DESC */   
         WK083 = WK001;         /* DISABILITY AGE */
         WK084 = WK002;         /* DISABILITY SVC */
         OCSHOW_LINE(TX004 ' / AGE ' WK083 ' / SVC ' WK084);
      END;
      IF TX003 = 'ALT DISABILITY';     
         TX005 = TX003;         /* ALT DISABILITY DESC */
         WK085 = WK001;         /* DISABILITY AGE */
         WK088 = WK002;         /* DISABILITY SVC */
         OCSHOW_LINE(TX005 ' / AGE ' WK085 ' / SVC ' WK088);
      END;
      IF TX003 = 'ALT NORMAL';
         TX006 = TX003;         /* ALT NORM DESC */
         WK086 = WK001;         /* ALT NORM AGE */
         WK087 = WK002;         /* ALT NORM SVC */
         OCSHOW_LINE(TX006 ' / AGE ' WK086 ' / SVC ' WK087);
      END;
      IF TX003 = 'ALT EARLY';
         TX007 = TX003;         /* ALT EARLY DESC */
         WK089 = WK001;         /* ALT EARLY AGE */
         WK095 = WK002;         /* ALT EARLY SVC */
         OCSHOW_LINE(TX007 ' / AGE ' WK089 ' / SVC ' WK095);
      END;
      IF (TX003='99999') OR (WK001=99999) OR (WK002=99999); /* AUDIT EACH RECORD */
         RPLOG_ERROR(MSG:'AUL RETIREMENT FILE RECORD INCOMPLETE. ');
      END;
    END; /* TEXT DESC CANNOT BE BLANK */
  END; /* RETIREMENT RECORD CANNOT BE BLANK */
ENDLOOP;
**
******************************************
ROUTINE 'LOAD.CTRETIREMENT';
******************************************
OCSHOW_LINE('==========================CT.RETIREMENT FILE========================');
OCCLIP_CLEAR();
TXOBJ_TOCLIP(PLAN:TX001 FILENAME:'RETIREMENT' PREFIX:'CT');
**REMOVE ANY EXISTING BLANK LINES FROM CLIPBOARD
OCCLIP_VIEW();
LOOP WHILE OCCLIP_NEXT();
  WK011=1;
  TX015=OCTEXT_NONBLANK(OCCLIP_LINE());
  IF TX015='';
    WK009=OCCLIP_LINENUM();
    OCCLIP_DELLINE(LINE:WK009);
**    OCSHOW_LINE('REMOVED A BLANK LINE FROM CLIPBOARD.');
  END;
ENDLOOP;
**
******************************************
ROUTINE 'ADD.TOCLIP';
******************************************
WK008=1; /* LOOPER */
LOOP WHILE WK008<5;
  IF (WK008=1) AND (TX004<>'');       /* DISABILITY */
OCSHOW_LINE('>>DISABILITY');
     IF (WK083<>0);      
       TX009=TX004+' AGE= '+OCTEXT_TRIM(OCFMT(WK083 'Z2.2'));
       IF OCCLIP_FIND(TX009 POS:0) = 0;
         OCCLIP_ADDLINE(TX009);
       END;
     END;
     IF (WK084<>0);
       TX008=TX004+' SVC= '+OCTEXT_TRIM(OCFMT_INTEGER(WK084));
       IF OCCLIP_FIND(TX008 POS:0) = 0;
         OCCLIP_ADDLINE(TX008);
       END;
     END;
  ELSEIF (WK008=2) AND (TX005<>'');   /* ALT DISABILITY */
OCSHOW_LINE('>>ALT-DISABILITY');
     IF (WK085<>0);  
       TX009=TX005+' AGE= '+OCTEXT_TRIM(OCFMT(WK085 'Z2.2'));
       IF OCCLIP_FIND(TX009 POS:0) = 0;
         OCCLIP_ADDLINE(TX009);
       END;
     END;
     IF (WK088<>0);
       TX008=TX005+' SVC= '+OCTEXT_TRIM(OCFMT_INTEGER(WK088));
       IF OCCLIP_FIND(TX008 POS:0) = 0;
         OCCLIP_ADDLINE(TX008);
       END;
     END;
  ELSEIF (WK008=3) AND (TX006<>'');   /* ALT NORMAL */
OCSHOW_LINE('>>ALT-NORMAL');
     IF (WK086<>0);  
       TX009=TX006+' AGE= '+OCTEXT_TRIM(OCFMT(WK086 'Z2.2'));
       IF OCCLIP_FIND(TX009 POS:0) = 0;
         OCCLIP_ADDLINE(TX009);
       END;
     END;
     IF (WK087<>0);
       TX008=TX006+' SVC= '+OCTEXT_TRIM(OCFMT_INTEGER(WK087));
       IF OCCLIP_FIND(TX008 POS:0) = 0;
         OCCLIP_ADDLINE(TX008);
       END;
     END;
  ELSEIF (WK008=4) AND (TX007<>'');   /* ALT EARLY */
OCSHOW_LINE('>>ALT-EARLY');
     IF (WK089<>0);
       TX009=TX007+' AGE= '+OCTEXT_TRIM(OCFMT(WK089 'Z2.2'));
       IF OCCLIP_FIND(TX009 POS:0) = 0;
         OCCLIP_ADDLINE(TX009);
       END;
     END;
     IF (WK095<>0);
       TX008=TX007+' SVC= '+OCTEXT_TRIM(OCFMT_INTEGER(WK095));
       IF OCCLIP_FIND(TX008 POS:0) = 0;
         OCCLIP_ADDLINE(TX008);
       END;
     END;
  END;
  IF (TX009<>'');
    OCSHOW_LINE('TX009 AGE = ' TX009);
  END;
  IF (TX008<>'');
    OCSHOW_LINE('TX008 SVC = ' TX008);
  END;
  WK008=+1; /* INCREMENT LOOPER */
ENDLOOP;
**
******************************************
ROUTINE 'CLIPTO.CTRETIREMENT';
******************************************
**OCSHOW_LINE('CLIPTO.CTRETIREMENT');
OCSHOW_LINE('>>CT.RETIREMENT FILE REPLACED WITH CLIPBOARD CONTENTS.');
IF TX022='U';
  TXOBJ_FROMCLIPREP(PLAN:TX001 FILENAME:'RETIREMENT' PREFIX:'CT'); 
END;
**
******************************************
ROUTINE 'DELETE.AULRETIREMENT';    
******************************************
IF TX022='U';
  IF TXTFOBJ_GET(PLAN:TX001 PREFIX:'  ' FILENAME:'RETIREMENT')>0;
     OCSHOW_LINE('>>RETIREMENT FILE BEING DELETED FOR PLAN: ' TX001);
     TXTXOBJ_VIEW(PLAN:TX001 PREFIX:'  ' FILENAME:'RETIREMENT');
     LOOP WHILE TXTXOBJ_NEXT();
       TXTXOBJ_DELETE();
**     OCSHOW_LINE('DELETING RECORD ...');
     ENDLOOP;
     TXTFOBJ_DELETE();
**   OCSHOW_LINE('AULs RETIREMENT FILE DELETED.');
  END;
END;
**
******************************************
ROUTINE 'PM550.UPDATE';
******************************************
**OCSHOW_LINE('PM550.UPDATE');
OCSHOW_LINE('>>PM550 BEFORE = ' TX010);
IF (TX010='01,02') OR (TX010='01, 02');
  TX010='01';
ELSEIF TX010='02';
  TX010='';
END;
OCSHOW_LINE('>>PM550 AFTER  = ' TX010);
IF TX022='U';
  PMPMOBJ_SETDE(DENUM:550 VALUE:TX010);  /* UPDATE PM550 */
  IF PMPMOBJ_UPDATE()=0;
    TX014='ERROR OCCURRED UPDATING PM550.';
    PERFORM 'REPORT.DETAIL';
    OCSHOW_LINE('ONE-RCF-POSTCONV:   ERROR UPDATING PM550 : ' TX001);
  END;
END;
** 
******************************************
ROUTINE 'PL886.UPDATE';
******************************************
**OCSHOW_LINE('PL886.UPDATE');
OCSHOW_LINE('>>UPDATING PL886 T0 0');
IF TX022='U';
  PLPLOBJ_SETDE(DENUM:886 VALUE:0);  /* UPDATE PL886 */
  IF PLPLOBJ_UPDATE()=0;
    TX014='ERROR OCCURRED UPDATING PL886.';
    PERFORM 'REPORT.DETAIL';
    OCSHOW_LINE('ONE-RCF-POSTCONV:   ERROR UPDATING PL886 : ' TX001);
  END;
END;
**
******************************************
ROUTINE 'PART.LOOP';
******************************************
**OCSHOW_LINE('PART.LOOP');
PTPHOBJ_VIEW(PLAN:TX001);
LOOP WHILE PTPHOBJ_NEXT();
  TX012=PTPHOBJ_DE(007);
  TX013=OCTEXT_SUB(TX012 1 8);
  IF (TX013<>"99999999") AND (PTPHOBJ_NUMDE(618)=2); 
     PERFORM 'PART.SOURCES'; /* UPDATE PS200 AND PS210 */
     PERFORM 'PART.UPDATE';  /* UPDATE PH618 */
  END;
ENDLOOP;
**
******************************************
ROUTINE 'PART.UPDATE';
******************************************
PTPHOBJ_SETDE(DENUM:618 VALUE:0); /* UPDATE TO 0 */
OCSHOW_LINE('>>UPDATING PH618 TO 0 FOR PLAN : ' TX001 ' / PART : ' TX012);
IF PTPHOBJ_UPDATE()=0;
  TX014='ERROR OCCURRED UPDATING PH618 TO 0 FOR PARTICIPANT: '+TX012;
  PERFORM 'REPORT.DETAIL';
END;
**
******************************************
ROUTINE 'PART.SOURCES';
******************************************
**OCSHOW_LINE('PART.SOURCES');
PTPSOBJ_VIEW(PLAN:TX001 PARTID:TX012);
LOOP WHILE PTPSOBJ_NEXT();
  IF (PTPSOBJ_NUMDE(200)>0);  /* IF PS200 GT 0 */
       OCSHOW_LINE('>>UPDATING PS200 TO 0 FOR PLAN : ' TX001 ' / PART : ' TX012 ' | SRC : ' PTPSOBJ_DE(008));
     IF TX022='U';
       PTPSOBJ_SETDE(DENUM:200 VALUE:0); /* UPDATE TO 0 */
       IF PTPSOBJ_UPDATE()=0;
         TX014='ERROR OCCURRED UPDATING PS200 FOR PARTICIPANT: '+TX012;
         PERFORM 'REPORT.DETAIL';
       END;
     END;
  END;
  IF (PTPSOBJ_DE(210)='1');   /* IF PS210 EQ 1 */
     OCSHOW_LINE('>>UPDATING PS210 TO 0 FOR PLAN : ' TX001 ' / PART : ' TX012 ' | SRC : ' PTPSOBJ_DE(008));
     IF TX022='U';
       PTPSOBJ_SETDE(DENUM:210 VALUE:'0');  /* UPDATE TO 0 */
       IF PTPSOBJ_UPDATE()=0;
         TX014='ERROR OCCURRED UPDATING PS210 FOR PARTICIPANT: '+TX012;
         PERFORM 'REPORT.DETAIL';
       END;
     END;
  END;
ENDLOOP;
**     
******************************************
ROUTINE 'DISPLAY.CLIP';
******************************************
OCSHOW_LINE('=========================CLIPBOARD CONTENTS=========================');
OCCLIP_VIEW();
LOOP WHILE OCCLIP_NEXT();
  TX099=OCCLIP_LINE();
  WK100=OCCLIP_LINENUM();
  OCSHOW_LINE(TX099);
ENDLOOP;
OCSHOW_LINE('================================END=================================');
**
*******************************************************************************************************
ROUTINE 'REPORT.DETAIL';
*******************************************************************************************************
  IF (WK099 > 62) OR (WK099 = 0);
    IF WK099 > 62;
      OCPAGE_WRITE(RPTSEQ:991);
      PERFORM 'REPORT.HEADER';
    END;
    IF WK099 = 0;
      PERFORM 'REPORT.HEADER';
    END;
  END;
  OCPAGE_SET(TX001                   LINE:WK099 COL:001);
  OCPAGE_SET(TX014                   LINE:WK099 COL:021);
  WK099=+1;
**
*******************************************************************************************************
ROUTINE 'REPORT.HEADER';
*******************************************************************************************************
  OCPAGE_SET('RUN DATE : '           LINE:001 COL:001);
  OCPAGE_SET(OCFMT_DATE3(SD003)      LINE:001 COL:012);
  OCPAGE_SET('ENV/TYPE : '           LINE:001 COL:060);
  OCPAGE_SET(TX021                   LINE:001 COL:070);
  OCPAGE_SET(' OMNI 5.8 POST CONV - MODIFY CT.RETIREMENT with AUL RETIREMENT FILE CONTENTS' LINE:002 COL:005);
  OCPAGE_SET('PLAN'                  LINE:004 COL:001);
  OCPAGE_SET('MESSAGE'               LINE:004 COL:021);
  WK099=5;
**
*******************************************************************************************************
ROUTINE 'REPORT.TOTALS';
*******************************************************************************************************
WK099=+2;
OCPAGE_SET('TOTAL NUMBER OF PLANS REPORTED : '    LINE:WK099  COL:002);
OCPAGE_SET(OCFMT(WK022 'INTEGER5')                LINE:WK099  COL:034);
WK099=+2;
OCPAGE_SET('TOTAL NUMBER OF PLANS ERRORED  : '    LINE:WK099  COL:002);
OCPAGE_SET(OCFMT(WK023 'INTEGER5')                LINE:WK099  COL:034);
**
*******************************************************************************************************
** END
*******************************************************************************************************
