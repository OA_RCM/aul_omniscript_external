************************************************************************************
**
**    Text File Name: IND-POST-PVR1.txt
**
**    Script calling the calculator: ind_post_pvr.ksh
**
**    Created by                     Date         CC#
**    ========================     ========     ========
**    Gary Pieratt                 06/07/11     WMS6259
*************************************************************************************
SD080 = 999999999;
TX008=OCTEXT_GETENV('CC12139SC47');
IF OCFILE1_OPEN(NAME:TX008 MODE:'OUTPUT')=0;
   OCSHOW_LINE(" !! Error opening " TX008);
   QUIT;
END;
**
TX024=OCTEXT_GETENV('CC12139T966');
IF OCFILE2_OPEN(NAME:TX024 MODE:'OUTPUT')=0;
   OCSHOW_LINE(" !! Error opening " TX024);
   QUIT;
END;
**
* Create container to hold Plan, SSN, Folder Id, T877 SV210 value
OCDATA_CODESTROY(Name:'Pre-PriorVest-T877');
OCDATA_COCREATE(Name:'Pre-PriorVest-T877' Keyleng:15 Nums:1 TX40S:1);
**
*PLPLOBJ_VIEW(PLAN:'G30077');
*PLPLOBJ_VIEW(PLANLO:'C00000' PLANHI:'C19999');              /* Loop thru plans */
IF OCFILE3_OPEN(NAME:OCTEXT_GETENV('PLANLIST') MODE:'INPUT');
   LOOP WHILE OCFILE3_READ(INTO:TX001);
     TX001=OCTEXT_TRIM(TX001);
     IF TX001 > 'A00000';
     OCSHOW_LINE(TX001 '...');
     PLPLOBJ_VIEW(PLAN:TX001);
     LOOP WHILE PLPLOBJ_NEXT();
       VTFHOBJ_VIEW(PLAN:TX001 FILTFID:'*.T877');
       LOOP WHILE VTFHOBJ_NEXT();
         TX020=VTFHOBJ_DE(050);                /* File ID */
         VTTDOBJ_VIEW(PLAN:TX001 FILEID:TX020 TRAN:'877');
         LOOP WHILE VTTDOBJ_NEXT();
           TX002=VTTDOBJ_DE(070);        /* Part Id */
           WK001=VTTDOBJ_NUMDE(200);     /* NUM OF CARDS */
           WK002=1;
           WK003=801;                    /* FIRST CARD */
           TX005='';
           LOOP UNTIL WK002>WK001;
             TX021=VTTDOBJ_DE(WK003);
             IF OCSUB(TX021 1 3) = '210';    /* GET SV210 VALUE FROM T877 RECORD */
                TX005=OCSUB(TX021 5 5);
             END;
             WK002=+1;
             WK003=+1;
           ENDLOOP;
           IF TX005 > '';
              WK005=OCTEXT_TONUM(TX005);
              TX010=OCSUB(TX001 1 6)+OCSUB(TX002 1 9);                       /* CREATE KEY - PLAN + SSN */
              OCDATA_ITEMGET(Name:'Pre-PriorVest-T877' KEY:TX010);
              OCDATA_DESET(Name:'Pre-PriorVest-T877' DENUM:001 VALUE:WK005);            /* T877 SV210 */
              OCDATA_DESET(Name:'Pre-PriorVest-T877' DENUM:600 VALUE:TX020);            /* FOLDER ID  */
              OCDATA_ITEMUPDATE(Name:'Pre-PriorVest-T877');
           END;
         ENDLOOP;
       ENDLOOP;
     ENDLOOP;
     END;
   ENDLOOP;
ELSE;
   OCSHOW_LINE(" !! Error opening " OCTEXT_GETENV('PLANLIST'));
   QUIT;
END;
WK050=OCDATA_COITEMS(Name:'Pre-PriorVest-T877'); TX022='';
** READ CONTAINER
OCDATA_COGET(Name:'Pre-PriorVest-T877');
OCDATA_ITEMVIEW(Name:'Pre-PriorVest-T877');
LOOP WHILE OCDATA_ITEMNEXT(Name:'Pre-PriorVest-T877');
   TX011=OcData_ItemKey();          /*  */
   TX012=OCSUB(TX011 1 6);          /* PLAN FROM CONTAINER */
   TX013=OCSUB(TX011 7 15);         /* PARTICIPANT ID FROM CONTAINER */
   TX014=OCDATA_DETEXT(600);        /* FOLDER ID FROM CONTAINER */
   WK006=OCDATA_NUMDE(001);         /* T877 SV210 FROM CONTAINER */
   IF TX022 = TX012;
      PERFORM 'BUILD.REPORT1';
   ELSE;
      IF TX022 <> '';
         BARUN_PLEND();
      END;
      TX022=TX012;
      BARUN_PLSTART(PLAN:TX012);
*     load plan level fields in file
      PMPMOBJ_GET(PLAN:TX012 PRODUCTID:PL025);
      TX015=PMPMOBJ_DE(492);      /* PENSION TEAM CODE */
      PLPLOBJ_GET(PLAN:TX012);
      TX016=PLPLOBJ_DE(951);      /* ADMIN NAME */
      TX017=PLPLOBJ_DE(902);      /* PLAN NAME */
      PERFORM 'BUILD.REPORT1';
   END;
ENDLOOP;
IF TX022 <> '';
   BARUN_PLEND();
END;
OCFILE1_CLOSE();
OCFILE2_CLOSE();
OCFILE3_CLOSE();
GOBACK;
*******************************************************************************************************
** ROUTINE : BUILD.REPORT1
*******************************************************************************************************
ROUTINE 'BUILD.REPORT1';
   BARUN_PTSTART(PLAN:TX012 PARTID:TX013);
   IF PTPHOBJ_GET(PLAN:TX012 PARTID:TX013) = 1;
      TX018=PTPHOBJ_DE(662);       /* YEARS OF VESTING */
      WK007=PTPHOBJ_NUMDE(663);   /* SOURCE VESTED % */
      WK009=PF999@***;            /* MARKET VALUE */
      WK010=0;
      WK008=0;
      SVSVOBJ_VIEW(PLAN:TX012 PARTID:TX013);
         LOOP WHILE SVSVOBJ_NEXT();
            IF SVSVOBJ_NUMDE(130) = 0;
               IF SVSVOBJ_NUMDE(210) > 0;
                  WK010=+1;
                  IF WK010 = 1;
                     WK008=SVSVOBJ_NUMDE(210);  /* 1ST ACTIVE WITH SV210 > 0 */
                  END;
               END;
            END;
         ENDLOOP;
         IF WK010 > 1;
            TX019 = 'Y';                      /* MULTIPLE SV210 FLAG */
         ELSE;
            TX019 = 'N';
         END;
      WK018=(OCTEXT_TONUM(TX018) * 12);
      IF (WK018 <> WK008) or (WK008 <> WK006);
         PERFORM 'REPORT.DETAIL';
         PERFORM 'BUILD.T966CARDS';
      END;
   ELSE;
      OCSHOW_LINE('PTPH Not Found - Plan: ' TX012 ' Part: ' TX013);
   END;
   BARUN_PTEND();
GOBACK;
*******************************************************************************************************
** ROUTINE : REPORT.DETAIL
*******************************************************************************************************
ROUTINE 'REPORT.DETAIL';
  TX027='';TX023='';
  OCCSV_EMPTY();
  OCCSV_ADDFIELD(
      TX012                    /* PLAN             */
      TX015                    /* TEAM CODE - PM492 */
      TX016                    /* ADMIN NAME - PL951 */
      TX017                    /* PLAN NAME - PL902 */
      TX013                    /* PARTICIPANT  */
      TX014                    /* FOLDER ID - LAST T877 FOLDER FOUND*/
      WK006                    /* VALUE OF SV210 - LAST T877 FOLDER FOUND*/
      WK008                    /* PRE SV210 VALUE - 1ST ACTIVE SV210 VALUE*/
      TX019                    /* MULTIPLE SV210 FLAG */
      TX018                    /* PH662 */
      WK007                    /* PH663 */
      WK009                    /* BALANCE - PF999@***  */
      TX027                    /* POST PRIORVEST SV210 VALUE - NOT POPULATED BY THIS REPORT, MAKE SPACES */
      TX023              );    /* PH663 REPORT FLAG - NOT POPULATED BY THIS REPORT, MAKE SPACES */
  OCCSV_TOCLIP();
  OCFILE1_FROMCLIP();
GOBACK;
*******************************************************************************************************
** ROUTINE : BUILD.T966CARDS
*******************************************************************************************************
ROUTINE 'BUILD.T966CARDS';
TX025="96600   $TIHDR " + TX012 + TX013 + "     "  +  OCFMT(SD003 'DATE3');
OCFILE2_WRITE(TX025);
TX026="96601   IND-PRIORVEST                                      001 1";
OCFILE2_WRITE(TX026);
GOBACK;
**
