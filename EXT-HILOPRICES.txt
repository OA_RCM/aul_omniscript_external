********************************************************************************
**
**    Text File Name: EXT-HILOPRICES.txt
**
**    Runs external / once a year with rpt_omniFees.ksh process
**    
**    Pulls select priceids and creates extract file with the highest and lowest
**    UV for the current year
**
**    Outputs : HILOprices{rundate}.csv
**
**              PRICE ID1    TX003
**              PRICE ID2    TX002
**              LONG NAME    TX004
**              HI UV        TX009
**              HI NAV       TX006
**              HI TD        TX011
**              LO UV        TX008
**              LO NAV       TX005
**              LO TD        TX010
**
**    Variable List:
**
**    TX001  price file extract name
**    TX002  price id modified
**    TX003  OMNI price id
**    TX004  Long name   PRPR120
**    TX005  LO NAV
**    TX006  HI NAV
**    TX007  ** free
**    TX008  LO UV
**    TX009  HI UV
**    TX010  LO UV TD
**    TX011  HI UV TD
**    TX012  Extract file record
**    TX013  run date YYYY
**
**    WK001  starting trade date based upon run date year
**    WK002  ending trade date based upon run date year
**    WK003  LO NAV      PRDR120
**    WK004  HI NAV      
**    WK005  UV          PRDR100
**    WK006  LO UV       
**    WK007  HI UV
**    WK008  LO UV TD    PRDR009
**    WK009  HI UV TD
**    WK010  Number of daily price records examined for priceid
**    WK011  Number of price records included in extract file
**    WK100  Flag to indicate where prices were found for investment id
**
**
**
**
**    Created by                           Date         SMR
**    ==============================     ========     ========
**    Judy Stewart                       03/03/2009   WMS2194
**
********************************************************************************
** PROGRAM INITIALIZATIONS
**
SD080=999999999;
**
** YEAR BASED UPON RUN DATE 
**
WK001=OCDATE_SET(YEAR:OCDATE_YEAR(SD003) MONTH:1 DAY:1);
WK002=SD003;
**
** OVERRIDE FOR UNIT TESTING
**
**WK001=20080101;
**WK002=20081231;
**
OCSHOW_LINE('REPORT START & STOP DATES : ' OCFMT_DATE4(WK001) ' - ' OCFMT_DATE4(WK002));
** 
** OPEN EXTRACT FILE
**
TX013=OCFMT_DATE3(WK002);         /* YYYYMMDD */
TX001=OCTEXT_GETENV('POSTOUT')+'/HILOprices_'+OCTEXT_SUB(TX013 1 8)+'.csv';
**TX001=OCTEXT_GETENV('XJOB')+'/HILOprices.csv';
IF OCFILV1_OPEN(NAME:TX001 MODE:'OUTPUT')=0; /* Create output file */
  RPLOG_ERROR(MSG:('EXT-HILOPRICES : Could not create extract price file - ' + TX001));
  QUIT;
ELSE;
  TX007="Price_Id_Long,Price_Id_Short,Price_Id_Name,HI_UV,HI_NAV,HI_Trade_Date,LO_UV,LO_NAV,LO_Trade_Date";
  IF OCFILV1_WRITE(TX007);
  ELSE;
    OCSHOW_LINE('!!! Error writing ' TX007);
  END;
END;
**
OCSHOW_LINE('Creating Priceid Extract file...');
OCSHOW_LINE('Extract price file name : ' TX001);
**
** READ PRICES IDS
**
PRPROBJ_VIEW(PLAN:'000001');             /* Read Priceids */
LOOP WHILE PRPROBJ_NEXT();
  IF OCFIND(OCSUB(PRPROBJ_DE(007) 5 6) 'AULVIP' 'AULSS1' 'AULARC' 'AULVPA' 'AULVPB'
    'AULVPC' 'AULVPD' 'AUL150' 'CYBVUL' 'CYBIVA' 'CYBLAN' 'CYBIVB');
    OCCLEAR(WK003 WK004 WK005 WK006 WK007 WK008 WK009 WK010);    /* CLEAR HI LO WORK VARS */
    WK100=0;
    PERFORM "GET.DAILYPRICES";
    IF WK100=1;  /* IF HI OR LOW UNIT VALUE CAPTURED */
      TX003=PRPROBJ_DE(007);   /* ACTUAL PRICE ID */
      TX002=OCSUB(PRPROBJ_DE(007) 1 4)+OCSUB(PRPROBJ_DE(007) 8 3); /* OMNI CYBR NAME */
      TX004=PRPROBJ_DE(120);   /* LONG NAME */
**    OCSHOW_LINE('Examining Priceid# : ' OCTEXT_TRIM(PRPROBJ_DE(007)) '.  # of Price records examined: ' WK010);
**    OCSHOW_LINE('Hi TD_UV_NAV : ' OCFMT_DATE4(WK009) ' | ' OCFMT_SHARE3(WK007) ' | ' OCFMT_SHARE3(WK004));
**    OCSHOW_LINE('Lo TD_UV_NAV : ' OCFMT_DATE4(WK008) ' | ' OCFMT_SHARE3(WK006) ' | ' OCFMT_SHARE3(WK003));
      PERFORM 'WRITE.EXTRACT';
    END;
  END;
ENDLOOP;
**
** CLOSE EXTRACT FILE
** 
OCFILv1_CLOSE();
OCSHOW_LINE('Priceid Extract file created.  Total number of priceids: ' OCFMT_INTEGER(WK011));
**********************************************************************************************
ROUTINE "GET.DAILYPRICES";
PRDROBJ_VIEW(PLAN:'000001' PRICEID:PRPROBJ_DE(007) TRADEDATELO:WK001 TRADEDATEHI:WK002); 
LOOP WHILE PRDROBJ_NEXT();       /* LOOP THROUGH ALL PRICES */
  WK010=+1;
  WK005=PRDROBJ_NUMDE(100);      /* CURRENT UNIT VALUE */
  IF (WK005<WK006) OR (WK006=0); /* CAPTURE LO UV */
    WK100=1;
    WK006=WK005;
    WK008=PRDROBJ_NUMDE(009);    /* LO TRADE DATE */
    WK003=PRDROBJ_NUMDE(120);    /* NAV */
  END;
  IF (WK005>WK007);              /* CAPTURE HI UV */
    WK100=1;
    WK007=WK005;
    WK009=PRDROBJ_NUMDE(009);    /* HI TRADE DATE */
    WK004=PRDROBJ_NUMDE(120);    /* NAV */
  END;
**  OCSHOW_LINE("| "PRPROBJ_DE(007) " | " WK005 " | " PRDROBJ_NUMDE(009) " | " PRDROBJ_NUMDE(120));
ENDLOOP;
IF (WK008=0) AND (WK009=0); /* if no hi or lo unit value found skip when writing to file */
   WK100=0;
END;
GOBACK;
**********************************************************************************************
ROUTINE "WRITE.EXTRACT";
WK011=+1;
TX005=OCFMT_SHARE3(WK003); /* LO NAV */
TX006=OCFMT_SHARE3(WK004); /* HI NAV */
TX008=OCFMT_SHARE3(WK006); /* LO UV */
TX009=OCFMT_SHARE3(WK007); /* HI UV */
TX010=OCFMT_DATE4(WK008);  /* LO UV TD */
TX011=OCFMT_DATE4(WK009);  /* HI UV TD */
OCTEXT_SETLENG(TX002 7);   /* MODIFIED PRICE ID */
OCTEXT_SETLENG(TX005 21);  /* SET MAX LENGTHS OF ALL NAV UVS */ 
OCTEXT_SETLENG(TX006 21);
OCTEXT_SETLENG(TX008 21);
OCTEXT_SETLENG(TX009 21);
OCTEXT_SETLENG(TX010 10);
OCTEXT_SETLENG(TX011 10);
TX012='"'+TX003+'"'+','+'"'+TX002+'"'+","+'"'+TX004+'"'+","+'"'+TX009+'"'+","+'"'+TX006+'"'+","+'"'+
      TX011+'"'+","+'"'+TX008+'"'+","+'"'+TX005+'"'+","+'"'+TX010+'"';
IF OCFILV1_WRITE(TX012);
ELSE;
   OCSHOW_LINE('!!! Error writing ' TX012);
END;
GOBACK;
